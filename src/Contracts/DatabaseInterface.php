<?php

namespace Framework\Contracts;

interface DatabaseInterface
{
    public function escapeString($str);

    public function fetch($query, $params, $single = false);

    public function insert($table, $params);

    public function update($table, $params, $where);

    public function delete($table, $where);

    public function execute($query, $params);

    public function beginTransaction();

    public function commit();

    public function rollback();
}
