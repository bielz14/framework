<?php

namespace Framework\Database;

interface EntityInterface
{
    public function getProperties();

    public function getTableName();

    public function isNew();

    public function save();
}
