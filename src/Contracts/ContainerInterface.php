<?php

namespace Framework\Contracts;

interface ContainerInterface
{
    public function get(string $name);

    public function has(string $name);

    public function set(string $name, $data);

    public function singleton(string $alias, $arguments = null);

    public function instance(string $alias, $instance);

    public function make(string $alias, $arguments = null);

    public function alias(...$arguments);

    public function resolving(string $alias, $callback);
}
