<?php

namespace Framework\Security\Entity;

use Framework\Database\Entity;

class Role extends Entity
{
    /**
     * @var int
     */
    public $parent_id;

    /**
     * @var string
     * @required
     */
    public $nick;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var Permission
     */
    protected $permissions;

    protected $accesses;

    /**
     * Related tables names
     * @var array
     */
    protected $links = ['permission', 'user'];


    /**
     * @return Entity|null
     */
    public function getParent()
    {
        if (empty($this->parent_id)) {
            return null;
        }
        return $this->getRepository()->findById($this->parent_id);
    }


    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->getRepository()->getRelations($this, User::class);
    }


    /**
     * @param Role $role
     * @return $this
     */
    public function clonePermissionsFrom(Role $role)
    {
        $this->unlinkAll(Permission::class);

        foreach ($role->getPermissions() as $permission) {
            $this->link($permission, [
                'access' => $role->getAccessLevel($permission->nick)
            ]);
        }
        return $this;
    }


    /**
     * @param $permission
     * @return int|mixed
     */
    public function getAccessLevel($permission)
    {
        if (empty($this->accesses)) {
            $this->accesses = [];
            $accesses = $this->getRepository()->getAccessLevels($this->id);

            foreach ($accesses as $access) {
                $this->accesses[$access['nick']] = $access['access'];
            }
        }
        return array_key_exists($permission, $this->accesses) ? $this->accesses[$permission] : 0;
    }


    /**
     * Update access level by permission nick
     *
     * @param $permission
     * @param $access
     * @return bool
     */
    public function setAccessLevel($permission, $access)
    {
        if (!$this->getPermission($permission)) {
            $permission = $this->getRepository(Permission::class)->find(['nick' => $permission], true);

            if (empty($permission)) {
                return false;
            }

            $this->link($permission, ['access' => $access]);
        } else {
            $this->link($this->getPermission($permission), [
                'access' => $access
            ]);
        }
        return true;
    }


    /**
     * @return array|Permission
     */
    public function getPermissions()
    {
        if (!$this->permissions) {
            $this->permissions = [];
            $permissions = $this->getRepository()->getRelations($this, Permission::class);

            foreach ($permissions as $permission) {
                $this->permissions[$permission->nick] = $permission;
            }
        }
        return $this->permissions;
    }


    /**
     * @param $permission
     * @return null
     */
    public function getPermission($permission)
    {
        return array_key_exists($permission, $this->getPermissions()) ? $this->permissions[$permission] : null;
    }

}
