<?php

namespace Framework\Security\Entity;

use Framework\Database\Entity;

class Permission extends Entity
{
    /**
     * @var string
     */
    public $nick;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

}
