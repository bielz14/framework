<?php

namespace Framework\Security\Entity;

use Framework\Database\Entity;
use Framework\Security\AuthService;
use Framework\Translation\LocalizedException;

class User extends Entity
{
    /**
     * @var string
     * @required
     */
    public $email;

    /**
     * @var string
     * @required
     */
    public $password;

    /**
     * @var int
     */
    public $temporary;

    /**
     * @var int
     */
    public $removed;

    /**
     * @var string
     */
    public $created;

    /**
     * @var Role
     */
    protected $role;

    /**
     * @var array
     */
    protected $links = ['role'];


    /**
     * @return Role
     */
    public function getRole()
    {
        if (!$this->role) {
            $roles = $this->getRepository()->getRelations($this, Role::class);
            $this->role = array_shift($roles);
        }
        return $this->role;
    }


    /**
     * @return array
     */
    public function getRoleTree()
    {
        return $this->getRepository(Role::class)->tree([], $this->getRole()->parent_id);
    }


    /**
     * @return array
     */
    public function getRoleList()
    {
        return $this->getRepository(Role::class)->children([], $this->getRole()->id);
    }


    /**
     * @param $permission
     * @return int|mixed
     */
    public function getAccessLevel($permission)
    {
        return $this->getRole()->getAccessLevel($permission);
    }


    /**
     * Return TRUE if user's role has READ access for current permission
     *
     * @param $permission
     * @return bool
     */
    public function canRead($permission)
    {
        return $this->getAccessLevel($permission) >= AuthService::PERMISSION_READ;
    }


    /**
     * @param $permission
     * @return bool
     */
    public function canEdit($permission)
    {
        return $this->getAccessLevel($permission) == AuthService::PERMISSION_WRITE;
    }


    /**
     * @param bool $exception
     * @return bool
     * @throws LocalizedException
     */
    public function checkRequiredProperties($exception = false)
    {
        if (filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {
            throw new LocalizedException('Неправильно заполнен E-mail');
        }
        return parent::checkRequiredProperties($exception);
    }

}
