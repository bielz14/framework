-- @section statements
-- you can write sql query here

CREATE TABLE `role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` INT UNSIGNED,
  `nick` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255),
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

ALTER TABLE `role` ADD UNIQUE INDEX `UNIQUE_ROLE_NICK` (`nick`);

-- @section rollback
-- you can write sql query for rollback migration here


