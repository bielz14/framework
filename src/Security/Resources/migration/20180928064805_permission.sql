-- @section statements
-- you can write sql query here

CREATE TABLE `permission` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nick` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255),
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here


