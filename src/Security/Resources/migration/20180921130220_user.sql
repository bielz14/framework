-- @section statements
-- you can write sql query here

CREATE TABLE `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `removed` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `temporary` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here


