-- @section statements
-- you can write sql query here

CREATE TABLE `role_user` (
  `user_id` INT UNSIGNED NOT NULL,
  `role_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here


