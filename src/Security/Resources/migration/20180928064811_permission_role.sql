-- @section statements
-- you can write sql query here

CREATE TABLE `permission_role` (
  `permission_id` INT UNSIGNED NOT NULL,
  `role_id` INT UNSIGNED NOT NULL,
  `access` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`role_id`, `permission_id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here


