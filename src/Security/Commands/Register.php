<?php

namespace Framework\Security\Commands;

use Framework\Database\DatabaseService;
use Framework\Command\Command;
use Framework\Security\AuthService;
use Framework\Security\Entity\User;
use Framework\Translation\LocalizedException;

class Register extends Command
{
    public $email;
    public $password;


    /**
     * Command entry point
     *
     * @param DatabaseService $db
     * @param AuthService $auth
     * @return array
     * @throws LocalizedException
     */
    public function run(DatabaseService $db, AuthService $auth)
    {
        if (is_string($this->email)) {
            $this->email = trim($this->email);
        }

        $this->validateParams();

        $user = $db->repository(User::class)->find([
            'email' => $this->email
        ], true);

        if ($user) {
            throw new LocalizedException('Пользователь с таким E-mail уже зарегистрирован в системе');
        }

        /** @var User $user */
        $user = $db->repository(User::class)->entity([
            'email' => $this->email,
            'password' => $auth->generatePassword($this->password)
        ])->save();

        return $this->success($user->asArray());
    }


    /**
     * Validate email and password
     *
     * @return bool
     * @throws \Exception
     */
    private function validateParams()
    {
        if (empty($this->email)){
            throw new LocalizedException('Не заполнено поле E-mail');
        }

        if (empty($this->password)){
            throw new LocalizedException('Не заполнено поле Пароль');
        }

        $pattern = '([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}';

        if (!preg_match($pattern, $this->email)) {
            throw new LocalizedException('Некорректный E-mail');
        }

        return true;
    }
}
