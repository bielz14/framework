<?php

namespace Framework\Security\Commands;

use Framework\Filesystem\FilesystemService;
use Framework\Command\Command;

class CreateCaptcha extends Command
{
    public $code;


    /**
     * Command entry point
     *
     * @param FilesystemService $fs
     * @return array
     */
    public function run(FilesystemService $fs)
    {
        $chars = 'abdefhknrstyz23456789';
        $length = rand(4, 5);
        $numChars = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, rand(1, $numChars) - 1, 1);
        }

        $array_mix = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
        srand ((float)microtime() * 1000000);
        shuffle ($array_mix);
        $code = implode("", $array_mix);

        $linenum = rand(3, 7);
        $font = $fs->getPath('public/fonts/font-awesome/fonts/fontawesome-webfont.ttf');
        $im = imagecreatetruecolor(150, 50);
        $text_color = imagecolorallocate($im, rand(0, 200), 0, rand(0, 200));
        imagefill($im, 0, 0, $text_color);
        imagefilter($im, IMG_FILTER_NEGATE);
        imagefilter($im, IMG_FILTER_BRIGHTNESS,-90);
        imagefilter($im, IMG_FILTER_COLORIZE, 150, 150, 150);
        for ($i = 0; $i < $linenum; $i++) {
            $color = imagecolorallocate($im, rand(0, 150), rand(0, 100), rand(0, 150));
            imageline($im, rand(0, 20), rand(1, 50), rand(150, 180), rand(1, 50), $color);
        }
        $x = rand(0, 35);
        for($i = 0; $i < strlen($code); $i++) {
            $x += 15;
            $letter = substr($code, $i, 1);
            $f = imagettftext($im, rand(12, 14), rand(2, 4), $x, rand(25, 35), $text_color, $font, $letter);
        }
        for ($i = 0; $i < $linenum; $i++) {
            $color = imagecolorallocate($im, rand(0, 255), rand(0, 200), rand(0, 255));
            imageline($im, rand(0, 20), rand(1, 50), rand(150, 180), rand(1, 50), $color);
        }
        ob_start();
        imagepng($im);
        $result = base64_encode(ob_get_clean());
        imagedestroy($im);

        return $this->success(['image' => $result, 'font' => $font]);
    }
}
