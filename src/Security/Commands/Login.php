<?php

namespace Framework\Security\Commands;

use Framework\Database\DatabaseService;
use Framework\Event\EventService;
use Framework\Command\Command;
use Framework\Security\AuthService;
use Framework\Security\Entity\User;
use Framework\Translation\LocalizedException;

class Login extends Command
{
    public $email;
    public $password;


    /**
     * Command entry point
     *
     * @param DatabaseService $db
     * @param AuthService $auth
     * @param EventService $event
     * @return array
     * @throws LocalizedException
     */
    public function run(DatabaseService $db, AuthService $auth, EventService $event)
    {
        if (is_string($this->email)) {
            $this->email = trim($this->email);
        }

        $this->validateParams();

        $user = $db->repository(User::class)->find([
            'email' => $this->email,
            'temporary' => 0,
            'removed' => 0,
        ], true);

        if (!$user) {
            throw new LocalizedException('Неправильный Email или пароль');
        }

        if (empty($user->password)) {
            throw new LocalizedException('Пароль для учётной записи не задан. Обратитесь к руководителю подразделения');
        }

        if (!$auth->validatePassword($this->password, $user->password)) {
            throw new LocalizedException('Неправильный Email или пароль');
        }

        $auth->setUser($user);
        $event->fire('login', ['user' => $user]);

        return $this->success();
    }


    /**
     * Validate email and password
     *
     * @return bool
     * @throws \Exception
     */
    private function validateParams()
    {
        if (empty($this->email)){
            throw new LocalizedException('Не заполнено поле Email');
        }

        if (empty($this->password)){
            throw new LocalizedException('Не заполнено поле Пароль');
        }

        $pattern = '/([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}/im';

        if (!preg_match($pattern, $this->email)) {
            throw new LocalizedException('Некорректный Email');
        }

        return true;
    }
}
