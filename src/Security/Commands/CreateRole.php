<?php

namespace Framework\Security\Commands;

use Framework\Database\DatabaseService;
use Framework\Command\Command;
use Framework\Security\Entity\Role;

class CreateRole extends Command
{
    public $nick;
    public $name;
    public $description;


    /**
     * Command entry point
     *
     * @param DatabaseService $db
     * @return array
     */
    public function run(DatabaseService $db)
    {
        /** @var Role $role */
        $role = $db->repository(Role::class)->entity([
            'nick' => $this->nick,
            'name' => $this->name,
            'description' => $this->description
        ])->save();

        return $this->success($role->asArray());
    }
}
