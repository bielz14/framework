<?php

namespace Framework\Security\Commands;

use Framework\Command\Command;
use Framework\Security\AuthService;

class Logout extends Command
{
    /**
     * Command entry point
     *
     * @param AuthService $auth
     * @return array
     */
    public function run(AuthService $auth)
    {
        $auth->setUser(null);
        return $this->success();
    }
}
