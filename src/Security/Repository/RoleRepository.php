<?php

namespace Framework\Security\Repository;

use Framework\Database\Repository;

class RoleRepository extends Repository
{
    /**
     * @param $role_id
     * @param bool $accessed
     * @return mixed
     */
    public function getAccessLevels($role_id, $accessed = false)
    {
        $aquery = $accessed ? ' AND rp.access > 0' : '';
        $query = 'SELECT p.*, rp.access FROM `permission_role` rp JOIN `permission` p on p.id = rp.permission_id WHERE `role_id` = :role_id' . $aquery;

        return $this->db->fetch($query, ['role_id' => $role_id]);
    }

}
