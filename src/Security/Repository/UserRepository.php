<?php

namespace Framework\Security\Repository;

use Framework\Database\Repository;
use Framework\Security\Entity\Role;

class UserRepository extends Repository
{
    /**
     * @param $user_id
     * @return array
     */
    public function findUserRoles($user_id)
    {
        $query = 'SELECT r.* FROM `role_user` ru JOIN `role` r on r.id = ru.role_id WHERE `user_id` = :user_id';

        $roles = $this->db->fetch($query, [
            'user_id' => $user_id
        ]);

        return $this->db->repository(Role::class)->entities($roles);
    }

}
