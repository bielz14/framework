<?php

namespace Framework\Security;

use Framework\Database\DatabaseService;
use Framework\Command\CommandService;
use Framework\Security\Entity\User;
use Framework\Session\SessionService;

class AuthService
{
    /** @var User */
    protected $user;

    private $database;
    private $command;
    private $session;

    const PERMISSION_NONE = 0;
    const PERMISSION_READ = 1;
    const PERMISSION_WRITE = 2;


    /**
     * AuthService constructor.
     *
     * @param CommandService $command
     * @param SessionService $session
     * @param DatabaseService $db
     */
    public function __construct(CommandService $command, SessionService $session, DatabaseService $db)
    {
        $this->session = $session;
        $this->database = $db;
        $this->command = $command;
    }


    /**
     * Logout from session
     */
    public function logout()
    {
        $this->command->run('security:logout');
    }


    /**
     * Set current user entity
     *
     * @param $user
     */
    public function setUser($user)
    {
        $this->session->user_id = !empty($user) ? $user->id : null;
    }


    /**
     * Get current user entity
     *
     * @return User
     */
    public function getUser()
    {
        if (!$this->user) {
            if (!$this->session->user_id) {
                return null;
            }
            $this->user = $this->database->repository(User::class)->findById($this->session->user_id);
        }
        return $this->user;
    }


    /**
     * Is user authorised in system
     *
     * @return bool
     */
    public function isAuthorized()
    {
        return $this->session->user_id !== null && $this->getUser();
    }


    /**
     * Generate random password
     *
     * @param int $length
     * @return bool|string
     */
    public function randomPassword($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    /**
     * Get password hash
     *
     * @param $password
     * @return string
     */
    public function generatePassword($password)
    {
        return md5($password . '~fDRe452&');
    }


    /**
     * Return true if password match the hash
     *
     * @param $password
     * @param $hash
     * @return bool
     */
    public function validatePassword($password, $hash)
    {
        return $this->generatePassword($password) == $hash;
    }


    /**
     * Generate hash for remind password
     *
     * @return string
     */
    public function generateRemindHash()
    {
        $hash = md5(time() . '~fDRe452&' . rand(0, 500));
        return $hash;
    }

}
