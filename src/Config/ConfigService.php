<?php

namespace Framework\Config;

use Framework\Filesystem\FilesystemService;

class ConfigService
{
    protected $path;
    protected $environments;
    protected $configurations;
    private $fs;


    /**
     * ConfigService constructor.
     *
     * @param FilesystemService $fs
     * @param string $path
     */
    public function __construct(FilesystemService $fs, $path = 'resources/config')
    {
        $this->path = $path;
        $this->fs = $fs;
    }


    /**
     * Search config files
     */
    public function init()
    {
        foreach ($this->fs->searchFiles($this->path, '.php') as $file) {
            $info = pathinfo($file);
            $this->configurations[$info['filename']] = include($file);
        }
    }


    /**
     * Get config value by key
     *
     * @param $key
     * @param null $default
     * @return null
     */
    public function get($key, $default = null)
    {
        $chain = explode('.', $key);

        if (!isset($this->configurations)) {
            $this->init();
        }

        $config = &$this->configurations;

        for ($i = 0; $i < count($chain)-1; $i++) {
            if (!is_array($config)) {
                return $default;
            }
            $config = &$config[$chain[$i]];
        }
        $key = array_pop($chain);
        return isset($config) && array_key_exists($key, $config) ? $config[$key] : $default;
    }


    /**
     * Set config by key
     *
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $chain = explode('.', $key);
        $config = &$this->configurations;

        for ($i = 0; $i < count($chain)-1; $i++) {
            if (!array_key_exists($chain[$i], $config)) {
                $config[$chain[$i]] = [];
            }
            $config = &$config[$chain[$i]];
        }
        $config[array_pop($chain)] = $value;
    }


    /**
     * Get environment variable by key (or variable overrided in .env file)
     *
     * @param $key
     * @param null $default
     * @return array|false|mixed|null|string
     * @throws \Exception
     */
    public function env($key, $default = null)
    {
        $result = getenv($key);

        if ($result) {
            return $result;
        }

        if (!isset($this->environments)) {
            $this->environments = [];

            if (!$this->fs->exists('.env')) {
                return $default;
            }

            foreach (file($this->fs->getPath('.env'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) as $str) {
                $str = trim($str);
                if (strlen($str) && $str[0] != '#') {
                    list($param, $value) = explode('=', $str);
                    if (!is_null($param)) {
                        $this->environments[trim($param)] = trim($value);
                    }
                }
            }
        }

        if (array_key_exists($key, $this->environments)) {
            return $this->environments[$key];
        } elseif (!is_null($default)) {
            return $default;
        }

        throw new \Exception('Parameter ' . $key . ' is not defined');
    }

}
