<?php

namespace Framework\Mail;

use Framework\Filesystem\FilesystemService;

class MailService
{
    protected $mailer;


    /**
     * MailService constructor.
     */
    public function __construct()
    {
        $transport = new \Swift_SmtpTransport(config('mail.host'), config('mail.port'), config('mail.security'));
        $transport->setUsername(config('mail.username'));
        $transport->setPassword(config('mail.password'));

        $this->mailer = \Swift_Mailer::newInstance($transport);
    }


    /**
     * Send E-mail
     *
     * @param $email
     * @param $subject
     * @param $body
     * @param array $attachments
     * @param string $content_type
     * @return int
     */
    public function send($email, $subject, $body, $attachments = [], $content_type = 'text/html')
    {
        if ($content_type == 'text/html') {
            $body = nl2br($body);
        }

        $message = \Swift_Message::newInstance()
            ->setFrom(config('mail.sender'))
            ->setTo($email)
            ->setSubject($subject)
            ->setBody($body, $content_type);

        if (!empty($attachments)) {
            $fs = app()->singleton(FilesystemService::class);

            foreach ($attachments as $name => $filename) {
                if (empty($filename)) {
                    continue;
                }

                $attachment = new \Swift_Attachment($fs->getContents($filename), $name);
                $message->attach($attachment);
            }
        }

        return $this->mailer->send($message);
    }

}
