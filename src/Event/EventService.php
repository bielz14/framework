<?php

namespace Framework\Event;

class EventService
{
    protected $events = [];


    /**
     * Listen event
     *
     * @param $event
     * @param $callback
     * @param null $instance
     */
    public function listen($event, $callback, $instance = null)
    {
        if (!array_key_exists($event, $this->events)) {
            $this->events[$event] = [];
        }
        $this->events[$event][] = [$callback, $instance];
    }


    /**
     * Fire event with arguments and run event's callback functions for all listeners
     *
     * @param $event
     * @param array $arguments
     * @return bool
     */
    public function fire($event, $arguments = [])
    {
        if (!array_key_exists($event, $this->events)) {
            return false;
        }

        foreach ($this->events[$event] as $listener) {
            if (is_null($listener[1])) {
                app()->call($listener[0], $arguments);
            } else {
                app()->call($listener[1], $listener[0], $arguments);
            }
        }
        return true;
    }
}
