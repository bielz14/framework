<?php

namespace Framework\Console;

use Exception;
use Framework\Command\CommandService;

class ConsoleService
{
    protected $command;
    protected $arguments;
    private $commandService;


    /**
     * ConsoleService constructor.
     *
     * @param CommandService $commandService
     */
    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
        $this->parseArguments();
    }


    /**
     * Run command from command line
     */
    public function handle()
    {
        $result = $this->commandService->run($this->command, $this->arguments, true);

        if ($result['success']) {
            die($result['message']);
        } elseif (!empty($result)) {
            var_dump($result);
        }
    }


    /**
     * Parse command arguments
     *
     * @throws Exception
     */
    protected function parseArguments()
    {
        $args = $_SERVER['argv'];
        array_shift($args);

        if (!count($args)) {
            $args = ['help'];
        }

        $this->command = array_shift($args);

        if (!isset($this->command)) {
            throw new Exception('Empty command');
        }

        $this->arguments = [];

        foreach ($args as $argument) {
            $matches = [];
            $is_option = preg_match('~([a-z_]+)=(.+)~', $argument, $matches);
            if ($is_option && count($matches) == 3) {
                $this->arguments[$matches[1]] = $matches[2];
            } else {
                $this->arguments[] = $argument;
            }
        }
    }

}
