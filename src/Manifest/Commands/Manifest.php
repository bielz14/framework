<?php

namespace Framework\Manifest\Commands;

use Framework\Command\Command;
use Framework\Command\CommandService;
use Framework\Config\ConfigService;
use Framework\Filesystem\FilesystemService;

class Manifest extends Command
{
    public $action;

    private $path = 'public/build';
    private $files = [];
    private $manifest = 'resources/manifest.json';
    private $resource_path = '';

    /** @var FilesystemService */
    private $fs;
    /** @var CommandService */
    private $command;


    /**
     * Command entry point
     *
     * @param ConfigService $config
     * @param FilesystemService $fs
     * @param CommandService $command
     * @return array
     */
    public function run(ConfigService $config, FilesystemService $fs, CommandService $command)
    {
        $this->files = $config->get('manifest.files');
        $this->path = $config->get('manifest.build_path');
        $this->resource_path = $config->get('manifest.resource_path');
        $this->manifest = $config->get('manifest.manifest');
        $this->fs = $fs;
        $this->command = $command;
        $command->register('manifest-concat', Concat::class);

        if (empty($this->action) || $this->action == 'build') {
            return $this->build();
        } elseif ($this->action == 'remove') {

        }
        return $this->success();
    }


    /**
     * Generate manifest file and concatenate input files
     *
     * @return array
     */
    protected function build()
    {
        $manifest_fname = $this->fs->getPath($this->manifest);
        $manifest_exists = file_exists($manifest_fname);
        $manifest = $manifest_exists ? (array)json_decode(file_get_contents($manifest_fname)) : [];
        $result = [];

        $this->fs->makeDir($this->path);

        foreach ($this->files as $index => $file) {
            $unlink_original = false;

            if (is_array($file)) {
                $unlink_original = true;
                $orig_fullname = $this->fs->getPath($this->resource_path . DIRECTORY_SEPARATOR . $index);
                $files = [];
                foreach ($file as $fname) {
                    $files[] = $this->fs->getPath($this->resource_path . DIRECTORY_SEPARATOR . $fname);
                }
                $this->command->run('manifest-concat', [
                    'input_files' => $files,
                    'output_file' => $orig_fullname
                ]);
                $file = $index;
            }

            $orig_fullname = $this->fs->getPath($this->resource_path . DIRECTORY_SEPARATOR . $file);
            $info = pathinfo($file);

            if (!file_exists($orig_fullname)) {
                continue;
            }

            if (array_key_exists($file, $manifest)) {
                rename($this->fs->getPath($manifest[$file]), $this->fs->getPath($manifest[$file] . '.old'));
            }

            $time = substr(md5(file_get_contents($orig_fullname)), 0, 10);
            $path_hash = substr(md5($file), 0, 4);
            $new_file = $this->path . '/' . $path_hash. '_' . $time . '.' . $info['extension'];

            if (copy($orig_fullname, $this->fs->getPath($new_file)) === false) {
                if ($manifest_exists) {
                    file_put_contents($manifest_fname, json_encode($result));
                }
                return $this->failure('Failed');
            }

            if (isset($manifest[$file])) {
                unlink($this->fs->getPath($manifest[$file] . '.old'));
            }

            if ($unlink_original) {
                unlink($orig_fullname);
            }

            $result[$file] = $new_file;
            $this->debug('File ' . $result[$file] . ' created' . PHP_EOL);
        }

        file_put_contents($manifest_fname, json_encode($result));
        return $this->success(null, "Manifest has been updated");
    }


    /**
     * Get command name
     *
     * @return string
     */
    public static function getName()
    {
        return 'manifest';
    }


    /**
     * Get command short description for console
     *
     * @return string
     */
    public static function getNote()
    {
        return 'build manifest file';
    }


    /**
     * Get command description for console
     *
     * @return string
     */
    public static function getDescription()
    {
        return self::getNote() . PHP_EOL. PHP_EOL . 'php index.php manifest [build] | [remove]' . PHP_EOL . PHP_EOL .
            "\t" . str_pad('build', 14, " ", STR_PAD_RIGHT) . 'create or update manifest' . PHP_EOL .
            "\t" . str_pad('remove', 14, " ", STR_PAD_RIGHT) . 'remove manifest file and resources with revision';
    }

}
