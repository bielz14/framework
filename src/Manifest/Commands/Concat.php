<?php

namespace Framework\Manifest\Commands;

use Framework\Command\Command;
use Framework\Filesystem\FilesystemService;

class Concat extends Command
{
    public $input_files;
    public $output_file;


    /**
     * Command entry point
     *
     * @param FilesystemService $fs
     * @return array
     */
    public function run(FilesystemService $fs)
    {
        if (empty($this->input_files)) {
            return $this->failure('No files for concatenation');
        }

        foreach ($this->input_files as $index => $file) {
            $fs->putContents($this->output_file, $fs->getContents($file), $index > 0);
        }

        return $this->success();
    }


    /**
     * Get command name
     *
     * @return string
     */
    public static function getName()
    {
        return 'manifest-concat';
    }


    /**
     * Get command short description for console
     *
     * @return string
     */
    public static function getNote()
    {
        return 'concatenate files';
    }


    /**
     * Get command description for console
     *
     * @return string
     */
    public static function getDescription()
    {
        return self::getNote() . PHP_EOL. PHP_EOL . 'php index.php manifest-concat';
    }

}
