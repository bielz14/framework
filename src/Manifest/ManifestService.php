<?php

namespace Framework\Manifest;

use Framework\Config\ConfigService;
use Framework\Filesystem\FilesystemService;
use Framework\Http\HttpService;

class ManifestService
{
    /**
     * @var array
     */
    protected $manifest = [];

    private $build_return_path = '';


    /**
     * ManifestService constructor.
     *
     * @param FilesystemService $fs
     */
    public function __construct(FilesystemService $fs, ConfigService $config)
    {
        $this->build_return_path = $config->get('manifest.build_return_path');

        if ($fs->exists('resources/manifest.json')) {
            $this->manifest = (array)json_decode($fs->getContents('resources/manifest.json'));
        }
    }


    /**
     * Return full path for concatenated file
     *
     * @param string $path
     * @return string
     */
    public function path($path)
    {
        $path = ltrim($path, '/');
        $base = app()->singleton(HttpService::class)->getBasePath();

        if (array_key_exists($path, $this->manifest)) {
            $path = $base . '/' . trim($this->build_return_path, '/\\') . '/' . basename($this->manifest[$path]);
        } else {
            $path = $base . '/' . $path;
        }

        return str_replace('//', '/', $path);
    }

}
