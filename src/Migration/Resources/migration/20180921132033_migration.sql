-- @section statements
-- you can write sql query here

CREATE TABLE `migration` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `filename` VARCHAR(255) NULL DEFAULT NULL,
  `applied` DATETIME NOT NULL,
  `status` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here

