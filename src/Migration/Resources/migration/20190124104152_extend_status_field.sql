-- @section statements
-- you can write sql query here

ALTER TABLE `migration`
  CHANGE COLUMN `status` `status` TEXT NULL DEFAULT NULL AFTER `applied`;

-- @section rollback
-- you can write sql query for rollback migration here


