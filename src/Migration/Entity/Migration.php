<?php

namespace Framework\Migration\Entity;

use Framework\Database\Entity;

class Migration extends Entity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $applied;

    /**
     * @var string
     */
    public $status;

}
