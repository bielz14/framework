<?php

namespace Framework\Migration;

use DateTime;
use Framework\Filesystem\FilesystemService;

class Migration
{
    protected $name;
    protected $comment;
    protected $created;
    protected $statements;
    protected $rollback;
    private $fs;


    /**
     * Migration constructor.
     *
     * @param FilesystemService $fs
     */
    public function __construct(FilesystemService $fs)
    {
        $this->fs = $fs;
    }


    /**
     * Load file content and parse sections
     *
     * @param string $dir
     * @param string $filename
     */
    public function load(string $dir, string $filename)
    {
        $filepath = $this->fs->getPath($dir . DIRECTORY_SEPARATOR . $filename);
        $lines = file($filepath, FILE_SKIP_EMPTY_LINES);
        $section = '';
        $content = '';

        foreach ($lines as $line) {
            $line = trim($line);

            $matches = [];
            preg_match('~^-- @section ([A-Za-z_0-9]+)~', $line, $matches);
            if (count($matches) == 2) {
                if (strlen($section)) {
                    $this->setSection($section, trim(str_replace('\\', '\\\\', $content)));
                }
                $section = array_pop($matches);
                $content = '';
            }

            if (substr($line, 0, 2) == '--') {
                continue;
            }

            $content .= $line;
        }

        $this->setFilename($filename);
    }


    /**
     * Get name and date from filename
     *
     * @param $filename
     * @throws \Exception
     */
    public function setFilename($filename)
    {
        $matches = [];
        preg_match('~^([0-9]{14})_(.+).sql$~', $filename, $matches);

        if (!$matches || count($matches) < 3) {
            throw new \Exception('Wrong migration filename');
        }

        $this->setName($matches[2]);
        $this->setDate(DateTime::createFromFormat('YmdHis', $matches[1]));
    }


    /**
     * Save file
     *
     * @param string $dir
     */
    public function save(string $dir)
    {
        $filename = $this->getFullName();
        $content = $this->getSectionContent('statements', 'you can write sql query here');
        $content .= $this->getSectionContent('rollback', 'you can write sql query for rollback migration here');
        $this->fs->putContents($dir . DIRECTORY_SEPARATOR . $filename . '.sql', $content);
    }


    /**
     * Get full filename with date
     *
     * @return string
     */
    public function getFullName()
    {
        return  $this->getDate()->format('YmdHis') . '_' . $this->getName();
    }


    /**
     * Get migration name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set migration name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = is_string($name) ? $name : '';
    }


    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }


    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = is_string($comment) ? $comment : '';
    }


    /**
     * Get date of migration creation
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->created;
    }


    /**
     * Set date of migration creation
     *
     * @param DateTime $date
     */
    public function setDate(DateTime $date)
    {
        $this->created = $date;
    }


    /**
     * Get section content
     *
     * @param $section
     * @return string
     */
    public function getSection($section)
    {
        return isset($this->{$section}) ? $this->{$section} : '';
    }


    /**
     * Get all section queries (if it has some inserts for example)
     *
     * @param $section
     * @return array
     */
    public function getSectionStatements($section)
    {
        $content = $this->getSection($section);
        $statements = [];
        $statement = '';
        $status = 0;
        $quote = '';
        $len = strlen($content);

        for ($i = 0; $i < $len; $i++) {
            $char = $content[$i];

            switch ($status) {
                case 0:
                    if ($char == "'") {
                        $status = 1;
                        $quote = "'";
                    } elseif ($char == '"') {
                        $status = 1;
                        $quote = '"';
                    } elseif ($char == '`') {
                        $status = 1;
                        $quote = '`';
                    } elseif ($char == ';') {
                        $statement .= $char;
                        $statements[] = $statement;
                        $statement = '';
                        break;
                    }
                    $statement .= $char;
                    break;
                case 1:
                    $statement .= $char;
                    if ($char == $quote) {
                        $status = 0;
                    }
            }
        }

        if (strlen($statement)) {
            $statements[] = $statement;
        }

        return $statements;
    }


    /**
     * Set section content
     *
     * @param $section
     * @param $content
     */
    public function setSection($section, $content)
    {
        $this->{$section} = $content;
    }


    /**
     * Get section content with header and comment
     *
     * @param $section
     * @param null $comment
     * @return string
     */
    public function getSectionContent($section, $comment = null)
    {
        $header = "-- @section $section" . PHP_EOL . (is_null($comment) ?  '' : "-- $comment") . PHP_EOL . PHP_EOL . PHP_EOL;
        return $header . $this->getSection($section);
    }

}
