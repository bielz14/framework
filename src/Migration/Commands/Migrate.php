<?php

namespace Framework\Migration\Commands;

use Framework\Command\Command;
use Framework\Database\DatabaseService;
use Framework\Migration\Migration;
use Framework\Migration\MigrationService;
use Framework\Migration\Entity\Migration as MigrationEntity;

class Migrate extends Command
{
    public $dir;

    protected $entityManager;
    protected $migrationRepository;


    /**
     * Command entry point
     *
     * @param DatabaseService $databaseService
     * @param MigrationService $migrationService
     * @return array
     */
    public function run(DatabaseService $databaseService, MigrationService $migrationService)
    {
        /** @var Migration[] $migrations */
        $migrations = $migrationService->getMigrations($this->dir);

        if (!count($migrations)) {
            return $this->failure('There are no migration in repository');
        }

        $migrationRepository = $databaseService->repository(MigrationEntity::class);
        $applied = [];

        try {
            /** @var MigrationEntity $entities */
            $entities = $migrationRepository->find();

            foreach ($entities as $migrationEntity) {
                $applied[$migrationEntity->filename] = $migrationEntity;
            }
        } catch(\Throwable $e) {}

        $count = 0;

        foreach ($migrations as $migration) {
            $fullName = $migration->getFullName();
            $statements = $migration->getSectionStatements('statements');

            if (!count($statements)) {
                continue;
            }

            $entity = null;
            if (array_key_exists($fullName, $applied)) {
                if ($applied[$fullName]->status == 'applied') {
                    continue;
                }
                $entity = $applied[$fullName];
            }

            $databaseService->beginTransaction();

            foreach ($statements as $statement) {
                try {
                    $databaseService->execute($statement);
                } catch (\Throwable $e) {
                    $databaseService->rollback();
                    $this->updateEntity($entity, $fullName, 'failed: ' . $e->getMessage());
                    return $this->failure('Migration ' . $fullName . ' failed with message "' . $e->getMessage() . '"');
                }
            }

            try {
                $this->updateEntity($entity, $fullName, 'applied');
                $databaseService->commit();
                $count++;
                echo 'Migration ' . $fullName . ' applied' . PHP_EOL;
            } catch (\Throwable $e) {
                $databaseService->rollback();
                $this->updateEntity($entity, $fullName, 'failed: ' . $e->getMessage());
                return $this->failure('Migration ' . $fullName . ' failed with message "' . $e->getMessage() . '"');
            }
        }

        return $this->success(null, "$count migration applied");
    }


    /**
     * Update or create migration entity
     *
     * @param $entity
     * @param $filename
     * @param $status
     */
    protected function updateEntity($entity, $filename, $status)
    {
        $dt = (new \DateTime())->format('Y-m-d H:i:s');

        if (!isset($entity)) {
            $entity = app()->make(MigrationEntity::class);
            $entity->filename = $filename;
            $entity->applied = $dt;
            $entity->status = $status;
            $entity->save();
        } else {
            $entity->applied = $dt;
            $entity->status = $status;
            $entity->save();
        }
    }


    /**
     * Get command name
     *
     * @return string
     */
    public static function getName()
    {
        return 'migrate';
    }


    /**
     * Get command short description for console
     *
     * @return string
     */
    public static function getNote()
    {
        return 'apply new migration to database';
    }


    /**
     * Get command description for console
     *
     * @return string
     */
    public static function getDescription()
    {
        return self::getNote() . PHP_EOL. PHP_EOL . 'php index.php migrate' . PHP_EOL;
    }

}
