<?php

namespace Framework\Migration\Commands;

use Framework\Command\Command;
use Framework\Migration\MigrationService;

class Migration extends Command
{
    public $name;
    public $comment;


    /**
     * Command entry point
     *
     * @param MigrationService $migrationService
     * @return array
     */
    public function run(MigrationService $migrationService)
    {
        if (empty($this->name)) {
            return $this->failure('Migration name is not defined');
        }

        $migration = $migrationService->create($this->name, $this->comment ?? '');

        return $this->success(null,'Migration ' . $migration->getFullName() . ' has been created');
    }


    /**
     * Get command name
     *
     * @return string
     */
    public static function getName()
    {
        return 'migration';
    }


    /**
     * Get command short description for console
     *
     * @return string
     */
    public static function getNote()
    {
        return 'create new migration';
    }


    /**
     * Get command description for console
     *
     * @return string
     */
    public static function getDescription()
    {
        return 'create new migration';
    }

}
