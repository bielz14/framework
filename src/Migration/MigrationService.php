<?php

namespace Framework\Migration;

use Framework\Command\CommandService;
use Framework\Config\ConfigService;
use Framework\Filesystem\FilesystemService;

class MigrationService
{
    public $migrations = null;
    private $fs;
    protected $dir;


    /**
     * MigrationService constructor.
     *
     * @param CommandService $command
     * @param FilesystemService $fs
     * @param ConfigService $config
     */
    public function __construct(CommandService $command, FilesystemService $fs, ConfigService $config)
    {
        $this->fs = $fs;
        $this->dir = $config->get('migration.directory', 'resources/migration');
    }


    /**
     * Set work directory
     *
     * @param $dir
     * @return $this
     */
    public function setDirectory($dir)
    {
        $this->dir = $dir;
        return $this;
    }


    /**
     * Get work directory
     *
     * @return null
     */
    public function getDirectory()
    {
        return $this->dir;
    }


    /**
     * Find all migrations in directory
     *
     * @param null $dir
     * @return array|null
     */
    public function getMigrations($dir = null)
    {
        if (is_null($dir)) {
            $dir = $this->dir;
        }

        if (is_null($this->migrations)) {
            $this->migrations = [];

            foreach ($this->fs->searchFiles($dir, '*.sql') as $filename) {
                $info = pathinfo($filename);
                $migration = app()->make(Migration::class);
                $migration->load($dir, $info['basename']);
                $this->migrations[] = $migration;
            }

            usort($this->migrations, function($first, $second) {
                return $first->getDate() > $second->getDate();
            });
        }

        return $this->migrations;
    }


    /**
     * Create new migration
     *
     * @param $name
     * @param string $comment
     * @return object
     */
    public function create($name, $comment = '')
    {
        $migration = app()->make(Migration::class);
        $migration->setName($name);
        $migration->setComment($comment);
        $migration->setDate(new \DateTime());
        $migration->save($this->dir);
        $this->migrations[] = $migration;
        return $migration;
    }

}
