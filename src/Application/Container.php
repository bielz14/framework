<?php

namespace Framework\Application;

class Container
{
    protected $instances = [];
    protected $aliases = [];
    protected $binded = [];
    protected $callbacks = [];
    protected $variables = [];


    /**
     * Bind interface with class.
     * You can use interface class name for dependency injection. If you bind
     * it with your class, container substitute instance of your class by interface name in method arguments
     *
     * @param $interface
     * @param $class
     */
    public function bind($interface, $class)
    {
        $this->binded[$interface] = $class;
    }


    /**
     * Set alias for class name.
     * Array of aliases may be used as arguments or couple of params "alias - classname"
     *
     * @param array ...$arguments
     */
    public function alias(...$arguments)
    {
        if (!isset($this->aliases)) {
            $this->aliases = [];
        }

        if (!count($arguments)) {
            return;
        }

        if (is_array($arguments[0])) {
            foreach ($arguments[0] as $alias => $argument) {
                $this->aliases[$alias] = $argument;
            }
        } elseif (count($arguments) == 2) {
            $this->aliases[$arguments[0]] = $arguments[1];
        }
    }


    /**
     * Get instance by class name or alias
     *
     * @param string $alias
     * @param $instance
     */
    public function instance(string $alias, $instance)
    {
        $className = $this->getClassName($alias);
        $this->instances[$className] = $instance;
    }


    /**
     * Add callback function for instance creation
     *
     * @param string $alias
     * @param $callback
     */
    public function resolving(string $alias, $callback)
    {
        $className = $alias;

        if (isset($this->binded[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->binded[$className];
        }

        if (isset($this->aliases[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->aliases[$className];
        }

        if (isset($this->instances[$className]) && !($this->instances[$className] instanceof \Closure)) {
            $callback($this, $this->instances[$className]);
        } else {
            if (!isset($this->callbacks[$className])) {
                $this->callbacks[$className] = [];
            }
            $this->callbacks[$className][] = $callback;
        }
    }


    /**
     * Get singleton by class name or alias
     *
     * @param string $alias
     * @param null $arguments
     * @return bool|mixed
     */
    public function singleton(string $alias, $arguments = null)
    {
        $className = $alias;

        if (isset($arguments) && $arguments instanceof \Closure) {
            $className = $this->getClassName($alias);
            $this->instances[$className] = $arguments;
            return true;
        }

        if (isset($this->binded[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->binded[$className];
        }

        if (isset($this->aliases[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->aliases[$className];
        }

        if (isset($this->instances[$className]) && $this->instances[$className] instanceof \Closure) {
            $this->instances[$className] = $this->instances[$className]($this);
        }

        if (!isset($this->instances[$className]) && !isset($arguments)) {
            $this->instances[$className] = true;
            $this->instances[$className] = $this->make($className);
        }
        $result = &$this->instances[$className];
        return $result;
    }


    /**
     * Return existing instance of class or null
     *
     * @param $alias
     * @return mixed|null
     */
    protected function getInstance($alias)
    {
        $className = $alias;

        if (isset($this->binded[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->binded[$className];
        }

        if (isset($this->aliases[$className])) {
            $className = isset($this->instances[$className]) ? $className : $this->aliases[$className];
        }

        if (isset($this->instances[$className]) && !($this->instances[$className] instanceof \Closure)) {
            return $this->instances[$className];
        }
        return null;
    }


    /**
     * Get class name by alias
     *
     * @param $alias
     * @return mixed
     */
    public function getClassName($alias)
    {
        return isset($this->aliases[$alias]) ? $this->aliases[$alias] : $alias;
    }


    /**
     * Get binded class name by interface name
     *
     * @param $class
     * @return mixed
     */
    public function getBindedClassName($class)
    {
        return isset($this->binded[$class]) ? $this->binded[$class] : $class;
    }


    /**
     * Create instance of class by alias or class name and fill arguments of constructor from array and
     * with dependency injection
     *
     * @param string $alias
     * @param array $arguments
     * @return object
     */
    public function make(string $alias, $arguments = [])
    {
        $className = $this->getBindedClassName($this->getClassName($alias));

        $class = new \ReflectionClass($className);
        if (method_exists($className, '__construct') === false) {
            $instance = $class->newInstanceWithoutConstructor();
        } else {
            $args = $this->fillArguments($className, '__construct', $arguments);
            $instance = $class->newInstanceArgs($args);
        }

        $this->runCallbacks($className, $instance);

        return $instance;
    }


    /**
     * Call instance method or function and fill arguments with dependency injection
     *
     * @param $instance
     * @param $method
     * @param array $arguments
     * @return mixed
     */
    public function call($instance, $method, $arguments = [])
    {
        if ($instance instanceof \Closure) {
            $reflectionMethod = new \ReflectionFunction($instance);
            $arguments = $this->fillArguments($instance, $method);
            return $reflectionMethod->invokeArgs($arguments);
        }
        $class = get_class($instance);
        $reflectionMethod = new \ReflectionMethod($class, $method);
        $arguments = $this->fillArguments($class, $method, $arguments);
        return $reflectionMethod->invokeArgs($instance, $arguments);
    }


    /**
     * @param $className
     * @param $instance
     */
    protected function runCallbacks($className, &$instance)
    {
        if (isset($this->instances[$className]) && $this->instances[$className] instanceof \Closure) {
            $this->instances[$className] = $instance;
        }
        if (isset($this->callbacks[$className])) {
            foreach ($this->callbacks[$className] as $callback) {
                $callback($this, $instance);
            }
        }
    }


    /**
     * Fill method or function from arguments array with dependency injection
     *
     * @param $className
     * @param $methodName
     * @param array $args
     * @return array
     * @throws \Exception
     */
    public function fillArguments($className, $methodName, $args = [])
    {
        $result = [];

        if ($className instanceof \Closure) {
            $method = new \ReflectionFunction($className);
            $args = empty($methodName) ? [] : $methodName;
        } else {
            $method = new \ReflectionMethod($className, $methodName);
        }

        $params = $method->getParameters();

        foreach ($params as $param) {
            if (isset($args[$param->getName()])) {
                $result[$param->getName()] = $args[$param->getName()];
            } elseif ($param->getClass()) {
                $class = $param->getClass()->getName();
                $binded = $this->getBindedClassName($class);

                if (isset($this->instances[$class]) || isset($this->instances[$binded])) {
                    $singleton = $this->singleton($class);

                    if ($singleton === true) {
                        throw new \Exception('Class ' . $class . ' is not initialized else for using in ' . $className);
                    }

                    $result[$param->getName()] = $singleton;
                }/* elseif (!$param->isOptional()) {
//                    $result[$param->getName()] = $this->make($class);
                }*/ elseif ($param->isDefaultValueAvailable()) {
                    $result[$param->getName()] = $param->getDefaultValue();
                } else {
                    $result[$param->getName()] = $this->singleton($class);
//                    throw new \Exception('Class ' . $class . ' not defined in class ' . $className);
                }
            } elseif ($param->isDefaultValueAvailable()) {
                $result[$param->getName()] = $param->getDefaultValue();
            } elseif (!$param->isOptional()) {
                $result[$param->getName()] = null;
            }
        }
        return $result;
    }


    /**
     * Get saved variable by name
     *
     * @param $var
     * @return mixed|null
     */
    public function get($var)
    {
        return array_key_exists($var, $this->variables) ? $this->variables[$var] : null;
    }


    /**
     * Set varible by name
     *
     * @param $var
     * @param $value
     */
    public function set($var, $value)
    {
        $this->variables[$var] = $value;
    }

}
