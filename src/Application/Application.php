<?php

namespace Framework\Application;

use Framework\Command\Commands\Help;
use Framework\Config\ConfigService;
use Framework\Migration\Commands\Migrate;
use Framework\Command\CommandService;
use Framework\Console\ConsoleService;
use Framework\Http\HttpService;
use Framework\Filesystem\FilesystemService;
use Framework\Manifest\Commands\Manifest;
use Framework\Migration\Commands\Migration;

class Application extends Container
{
    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->instance(get_class($this), $this);

        $this->singleton(FilesystemService::class, function () {
            return app()->make(FilesystemService::class, ['directory' => app()->getRootDir()]);
        });
    }


    /**
     * Handle user request
     */
    public function handle()
    {
        $this->registerCommands();

        // Console or http request
        if ($this->isCliRunning()) {
            $this->singleton(ConsoleService::class)->handle();
        } else {
            $this->singleton(HttpService::class)->handle();
        }
    }


    /**
     * Is test environment
     *
     * @return bool
     */
    public function isTestEnv()
    {
        return $this->singleton(ConfigService::class)->get('application.env', 'prod') == 'test';
    }


    /**
     * Is development environment
     *
     * @return bool
     */
    public function isDevEnv()
    {
        return $this->singleton(ConfigService::class)->get('application.env', 'prod') == 'dev';
    }


    /**
     * Is production environment
     *
     * @return bool
     */
    public function isProdEnv()
    {
        return $this->singleton(ConfigService::class)->get('application.env', 'prod') == 'prod';
    }


    /**
     * Register console commands
     */
    protected function registerCommands()
    {
        $this->singleton(CommandService::class)->register([
            'migration' => Migration::class,
            'manifest' => Manifest::class,
            'migrate' => Migrate::class,
            'help' => Help::class
        ]);
    }


    /**
     * Check console or http request running
     *
     * @return bool
     */
    public function isCliRunning()
    {
        return defined('STDIN') || (empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0);
    }


    /**
     * Get current application root directory
     *
     * @return bool|string
     */
    public function getDir()
    {
        return realpath(__DIR__ . '/src/');
    }


    /**
     * Get project root directory
     *
     * @return bool|string
     */
    public function getRootDir()
    {
        return realpath(__DIR__ . '/../../lexsystems/');
    }


    /**
     * Get application namespace
     *
     * @return string
     */
    public function getNamespace()
    {
        return __NAMESPACE__;
    }


    /**
     * Get file namespace by directory. For example:
     *      /var/www/html/src/framework/Application/Application -> framework\Application\Application
     *      Application/Application -> framework\Application\Application (if root directory of FilesystemService is ../framework)
     *
     * @param $path
     * @return string
     */
    public function getNamespaceByPath($path)
    {
        $name = substr($this->getNamespace(), 0, strpos($this->getNamespace(), '\\'));
        $path = $this->singleton(FilesystemService::class)->getPath($path);
        $path = trim(str_replace($this->getRootDir(), '', $path), DIRECTORY_SEPARATOR);
        $chain = explode(DIRECTORY_SEPARATOR, $path);
        if (count($chain) && $chain[0] == 'source') {
            array_shift($chain);
        }
        $result = [];

        foreach ($chain as $index => $unit) {
            $result[] = ucfirst($unit);
        }

        return $name . '\\' . implode('\\', $chain);
    }

}
