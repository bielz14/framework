<?php

namespace Framework\Routing;

use Framework\Config\ConfigService;
use Framework\Http\HttpService;

class RoutingService
{
    protected $routes = [];
    protected $arguments = [];
    protected $controllerPath = 'Controller';


    /**
     * RoutingService constructor.
     *
     * @param ConfigService $config
     */
    public function __construct(ConfigService $config)
    {
        $methods = $config->get('routing.routes', []);

        foreach ($methods as $method => $routes) {
            foreach ($routes as $route => $controller) {
                $this->add($route, $controller, $method);
            }
        }
    }


    /**
     * Parse route string
     *
     * @param $route
     * @return array
     */
    public function parse($route)
    {
        $route_chain = explode('/', trim($route, '/'));
        $result = [];

        foreach ($route_chain as $index => $item) {
            if (preg_match('~{([a-z0-9\-_]+)(\?)?}(.+)?~', $item, $matches)) {
                array_shift($matches);
                $nick = array_shift($matches);
                $is_optional = array_shift($matches);
                $tpl = null;
                $is_static = false;

                if (!is_null($is_optional)) {
                    if ($is_optional == '?') {
                        $is_optional = true;
                        $tpl = array_shift($matches);
                        $tpl = !is_null($tpl) ? trim($tpl, '()') : null;
                    } else {
                        $tpl = trim($is_optional, '()');
                    }
                }

                $result[] = compact('nick', 'tpl', 'is_optional', 'is_static');

            } elseif (preg_match('~([a-z0-9\-_]+)~', $item, $matches)) {
                $result[] = [
                    'nick' => $matches[1],
                    'tpl' => null,
                    'is_static' => true,
                    'is_optional' => false
                ];
            }
        }
        return $result;
    }


    /**
     * Return Route object if url match route string or return False
     *
     * @param $url
     * @param null $method
     * @return bool|object
     */
    public function match($url, $method = null)
    {
        $http = app()->singleton(HttpService::class);
        $base = trim($http->getBasePath(), '/');
        $url = trim($url, '/');
        $url = str_replace($base, '', $url);
        $url = trim(str_replace('//', '/', $url), '/');
        $method = $method ? strtoupper($method) : $http->getRequest()->method;
        $anchor = '';

        if (strpos($url, '?') !== false) {
            $url = trim(substr($url, 0, strpos($url, '?')), '/');
        } elseif (preg_match('/^(.*)\#([A-Za-z\-_]+)$/', $url, $matches)) {
            $url = trim($matches[1], '/');
            $anchor = $matches[2];
        }

        $url_chain_orig = strlen($url) ? explode('/', $url) : [];

        foreach ($this->routes as $parsed) {
            $optional = [];

            if (count($parsed['chain']) < count($url_chain_orig)) {
                continue;
            }

            $result = true;
            $arguments = [];
            $url_chain = strlen($url) ? explode('/', $url) : [];

            foreach ($parsed['chain'] as $route_chain_index => $route_chain_item) {
                if (!$route_chain_item['is_static']) {
                    $optional[] = $route_chain_item;
                    continue;
                }

                $index = array_search($route_chain_item['nick'], $url_chain);

                if ($index === false || count($optional) < $index) {
                    $result = false;
                    break;
                }

                if (!$this->matchOptions($index-1, $optional, $url_chain, $arguments)) {
                    $result = false;
                    break;
                }

                $optional = [];
                $url_chain = array_splice($url_chain, $index + 1);
            }

            if (count($optional)) {
                if (!$this->matchOptions(count($url_chain)-1, $optional, $url_chain, $arguments)) {
                    $result = false;
                }
                $url_chain = [];
            }

            if (count($url_chain)) {
                $result = false;
            }

            if ($result) {
                list($controller, $_method) = explode('::', $parsed['controller']);

                return app()->make(Route::class, [
                    'args' => $arguments,
                    'controller' => $controller,
                    'method' => $_method,
                    'chain' => $parsed['chain'],
                    'params' => '',
                    'anchor' => $anchor
                ]);
            }
        }
        return false;
    }


    /**
     * @param $count
     * @param $optional
     * @param $url_chain
     * @param $args
     * @return bool
     */
    private function matchOptions($count, &$optional, &$url_chain, &$args)
    {
        $k = 0;
        $optional_count = count($optional);

        for ($i = 0; $i < $optional_count; $i++) {
            $exists = array_key_exists($k, $url_chain);

            if (!$optional[$i]['is_optional'] && !$exists) {
                return false;
            }

            if ($exists) {
                if (!is_null($optional[$i]['tpl'])) {
                    $matches = [];
                    if (preg_match('~^' . $optional[$i]['tpl'] . '$~', $url_chain[$k], $matches) === 1) {
                        $args[$optional[$i]['nick']] = $url_chain[$k];
                        $k++;
                    } elseif (!$optional[$i]['is_optional']) {
                        return false;
                    }
                } else {
                    $args[$optional[$i]['nick']] = $url_chain[$k];
                    $k++;
                }
            }

            if ($i == $optional_count - 1 && $k <= $count) {
                return false;
            }
        }
        return true;
    }


    /**
     * Insert arguments into url
     *
     * @param $url
     * @param array $args
     * @param string $method
     * @return string
     */
    public function generate($url, $args = [], $method = 'GET')
    {
        $route = $this->match($url, $method);

        if (!$route) {
            return '';
        }

        $result = '';
        $args = array_merge($route->getArguments(), $args);
        $anchor = $route->getAnchor();
        $params = $route->getSearchParams();

        foreach ($route->getChain() as $route_item) {
            if ($route_item['is_static']) {
                // Append to result item if it's static
                $result .= '/' . $route_item['nick'];
            } else {
                // Replace item with value from args if it's optional and value exists in array
                if (!$route_item['is_optional'] && !array_key_exists($route_item['nick'], $args)) {
                    return '';
                } elseif (isset($args[$route_item['nick']])) {
                    $result .= '/' . $args[$route_item['nick']];
                }
            }
        }

        $result = '/' . ltrim($result, '/');

        if (!empty($params)) {
            $result = rtrim($result, '/') . '/' . $route->getSearchString();
        } elseif (!empty($anchor)) {
            $result = rtrim($result, '/') . '/#' . $anchor;
        }

        return $result;
    }


    /**
     * Add route into route list
     *
     * @param $route
     * @param $controller
     * @param string $method
     */
    public function add($route, $controller, $method = 'GET')
    {
        $this->routes[] = [
            'route' => $route,
            'chain' => $this->parse($route),
            'controller' => $controller,
            'http_method' => $method
        ];
    }


    /**
     * @param $arg
     * @return mixed|null
     */
    public function get($arg)
    {
        return $this->arguments[$arg] ?? null;
    }


    /**
     * @param $arg
     * @param $value
     */
    public function set($arg, $value)
    {
        $this->arguments[$arg] = $value;
    }

}
