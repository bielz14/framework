<?php

namespace Framework\Routing;

class Route
{
    protected $url;
    protected $template;
    protected $args = [];
    protected $controller;
    protected $controllerMethod;
    protected $chain;
    protected $anchor = '';
    protected $params = [];


    /**
     * Route constructor.
     *
     * @param $controller
     * @param $method
     * @param $args
     * @param $chain
     * @param $anchor
     * @param $params
     */
    public function __construct($controller, $method, $args, $chain, $anchor, $params)
    {
        $this->controller = $controller;
        $this->controllerMethod = $method;
        $this->args = $args;
        $this->chain = $chain;
        $this->anchor = $anchor;
        $this->params = $params;
    }


    /**
     * Get anchor (starts with "#" in uri)
     *
     * @return string
     */
    public function getAnchor()
    {
        return $this->anchor ?? '';
    }


    /**
     * Get search params (starts with "?" in uri)
     *
     * @return array
     */
    public function getSearchParams()
    {
        return $this->params;
    }


    /**
     * @return string
     */
    public function getSearchString()
    {
        $result = [];

        if (empty($this->params)) {
            return '';
        }

        foreach ($this->params as $key => $value) {
            $result[] = $key . '=' . $value;
        }

        return '?' . implode('&', $result);
    }


    /**
     * @return mixed
     */
    public function getChain()
    {
        return $this->chain;
    }


    /**
     * Get route arguments (present as {argument})
     * @return array
     */
    public function getArguments()
    {
        return $this->args;
    }


    /**
     * @param $key
     * @param $value
     */
    public function setArgument($key, $value)
    {
        $this->args[$key] = $value;
    }


    /**
     * Get route arguments (present as {argument})
     *
     * @param $key
     * @return mixed|null
     */
    public function getArgument($key)
    {
        return array_key_exists($key, $this->args) ? $this->args[$key] : null;
    }


    /**
     * @param $controller
     */
    public function setControllerName($controller)
    {
        $this->controller = $controller;
    }


    /**
     * In what controller method defined
     *
     * @return mixed
     */
    public function getControllerName()
    {
        return $this->controller;
    }


    /**
     * @param $method
     */
    public function setControllerMethod($method)
    {
        $this->controllerMethod = $method;
    }


    /**
     * Get route handler
     *
     * @return mixed
     */
    public function getControllerMethod()
    {
        return $this->controllerMethod;
    }

}
