<?php

namespace Framework\Filesystem;

class FilesystemService
{
    protected $directory;


    /**
     * FilesystemService constructor.
     *
     * @param $directory
     */
    public function __construct($directory)
    {
        $this->directory = $directory;
    }


    /**
     * Get absolute path for Storage directory
     *
     * @param string $path
     * @return mixed|string
     */
    public function getStoragePath($path = '')
    {
        $path = $path ? '/' . $path : '';
        return $this->getPath(config('filesystem.storage_path', 'storage') . $path);
    }


    /**
     * Get absolute path for Public directory
     *
     * @param string $path
     * @return mixed|string
     */
    public function getPublicPath($path = '')
    {
        $path = $path ? '/' . $path : '';
        return $this->getPath(config('filesystem.public_path', 'public') . $path);
    }


    /**
     * Get absolute path for Resource directory
     *
     * @param string $path
     * @return mixed|string
     */
    public function getResourcePath($path = '')
    {
        $path = $path ? '/' . $path : '';
        return $this->getPath(config('filesystem.resource_path', 'resources') . $path);
    }


    /**
     * Get absolute path for directory or file
     *
     * @param $path
     * @return mixed|string
     */
    public function getPath($path)
    {
        $path = preg_replace('/[\/\\\]+/', DIRECTORY_SEPARATOR, $path);
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        if (preg_match('/^[a-z]:/', $path)) {
            $path = ucfirst($path);
        }

        if (strpos($path, app()->getRootDir()) === 0) {
            return $path;
        }

        return $this->directory . DIRECTORY_SEPARATOR . trim($path);
    }


    /**
     * Make directory if it not exists in server base directory
     *
     * @param string $path
     * @param $mode
     * @return boolean
     */
    public function makeDir($path, $mode = 0777)
    {
        if (!$this->exists($path)) {
            return mkdir($this->getPath($path), $mode, true);
        }
        return true;
    }


    /**
     * Delete file or directory with subdirectories
     *
     * @param $file
     * @return bool
     */
    public function delete($file)
    {
        $fullPath = $this->getPath($file);

        if (!$this->exists($fullPath)) {
            return true;
        }

        if (!is_dir($fullPath)) {
            return unlink($fullPath);
        }

        foreach (scandir($fullPath) as $fname) {
            if ($fname != '.' && $fname != '..') {
                $this->delete($file . DIRECTORY_SEPARATOR . $fname);
            }
        }
        return rmdir($fullPath);
    }


    /**
     * Move file to another directory and the same or another name
     *
     * @param $from
     * @param $to
     * @return bool
     */
    public function move($from, $to)
    {
        if (!$this->exists($this->getPath($from))) {
            return false;
        }

        if (copy($this->getPath($from), $this->getPath($to))) {
            return $this->delete($from);
        }
        return false;
    }


    /**
     * Reads entire file into a string
     *
     * @param $filename
     * @return bool|string
     */
    public function getContents($filename)
    {
        return file_get_contents($this->getPath($filename));
    }


    /**
     * Write a string to a file
     *
     * @param $filename
     * @param $content
     * @param bool $append
     * @return bool|int
     */
    public function putContents($filename, $content, $append = false)
    {
        return file_put_contents($this->getPath($filename), $content, $append ? FILE_APPEND : 0);
    }


    /**
     * Checks whether a file or directory exists
     *
     * @param $filename
     * @return bool
     */
    public function exists($filename)
    {
        return file_exists($this->getPath($filename));
    }


    /**
     * Find files by template
     *
     * @param $path
     * @param $filename
     * @return array
     */
    public function searchFiles($path, $filename)
    {
        $result = [];
        $path = $this->getPath($path);
        $files = scandir($path);
        $filename = str_replace('*', '[A-Za-z0-9_.]+', $filename);

        foreach ($files as $file) {
            if ($file == '..' || $file == '.') {
                continue;
            }

            $fullPath = $path . DIRECTORY_SEPARATOR . $file;

            if (is_dir($fullPath)) {
                $result = array_merge($result, $this->searchFiles($fullPath, $filename));
            } elseif (preg_match('~' . $filename . '~', $file)) {
                $result[] = $fullPath;
            }
        }
        return $result;
    }
}
