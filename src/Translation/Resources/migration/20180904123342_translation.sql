-- @section statements
-- you can write sql query here

CREATE TABLE `translation` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `language_id` INT UNSIGNED,
  `key` VARCHAR(255),
  `text` TEXT NOT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_bin'
  ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here

