-- @section statements
-- you can write sql query here

CREATE TABLE `language` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB;

INSERT INTO `language` (`name`) VALUES ('ru');
INSERT INTO `language` (`name`) VALUES ('lt');
INSERT INTO `language` (`name`) VALUES ('en');
INSERT INTO `language` (`name`) VALUES ('fr');
INSERT INTO `language` (`name`) VALUES ('si');
INSERT INTO `language` (`name`) VALUES ('bg');
INSERT INTO `language` (`name`) VALUES ('cz');
INSERT INTO `language` (`name`) VALUES ('hu');
INSERT INTO `language` (`name`) VALUES ('no');
INSERT INTO `language` (`name`) VALUES ('pl');
INSERT INTO `language` (`name`) VALUES ('pt');
INSERT INTO `language` (`name`) VALUES ('se');
INSERT INTO `language` (`name`) VALUES ('de');
INSERT INTO `language` (`name`) VALUES ('tr');
INSERT INTO `language` (`name`) VALUES ('ee');
INSERT INTO `language` (`name`) VALUES ('hr');
INSERT INTO `language` (`name`) VALUES ('nl');
INSERT INTO `language` (`name`) VALUES ('dk');
INSERT INTO `language` (`name`) VALUES ('gr');
INSERT INTO `language` (`name`) VALUES ('it');
INSERT INTO `language` (`name`) VALUES ('lv');
INSERT INTO `language` (`name`) VALUES ('ro');
INSERT INTO `language` (`name`) VALUES ('sk');
INSERT INTO `language` (`name`) VALUES ('hn');

-- @section rollback
-- you can write sql query for rollback migration here

