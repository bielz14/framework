<?php

namespace Framework\Translation\Entity;

use Framework\Database\DatabaseService;
use Framework\Database\Entity;
use Framework\Translation\TranslationService;

class LocalizedEntity extends Entity
{
    /**
     * LocalizedEntity constructor.
     *
     * @param DatabaseService $db
     * @param TranslationService $tr
     * @param array $data
     */
    public function __construct(DatabaseService $db, TranslationService $tr, array $data = [])
    {
        parent::__construct($db, $data);

        $translations = $tr->record($this->getTableName(), $this->id);

        foreach ($translations as $field => $value) {
            $this->{$field} = $value;
        }
    }

}
