<?php

namespace Framework\Translation\Entity;

use Framework\Database\Entity;

class Language extends Entity
{
    /**
     * @var string $name
     */
    public $name;

}
