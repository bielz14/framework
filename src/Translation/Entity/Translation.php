<?php

namespace Framework\Translation\Entity;

use Framework\Database\Entity;

class Translation extends Entity
{
    /**
     * @var int
     */
    public $language_id;

    /**
     * @var int
     */
    public $key;

    /**
     * @var string
     */
    public $text;

}
