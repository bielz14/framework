<?php

namespace Framework\Translation;

use Framework\Database\DatabaseService;
use Framework\Translation\Entity\Language;
use Framework\Translation\Entity\Translation;

class TranslationService
{
    /** @var Language */
    private $lang;
    /** @var DatabaseService */
    private $db;


    /**
     * TranslationService constructor.
     *
     * @param DatabaseService $db
     */
    public function __construct(DatabaseService $db)
    {
        $this->db = $db;
        $lang = config('application.language', 'ru');
        $this->setLanguage($lang);
    }


    /**
     * Find translation by string
     *
     * @param $text
     * @return string
     */
    public function translate($text)
    {
        /** @var Translation $entity */
        $entity = $this->db->repository(Translation::class)->find([
            'text' => $text
        ], true);

        if (!$entity) {
            return $text;
        }

        /** @var Translation $translated */
        $translated = $this->get($entity->key);

        return $translated ? $translated->text : $text;
    }


    /**
     * Get Translation entity by key and for current language
     *
     * @param $key
     * @return array|\Framework\Database\Entity|\Framework\Database\Entity[]|null
     */
    public function get($key)
    {
        return $this->db->repository(Translation::class)->find([
            'language_id' => $this->lang->id,
            'key' => $key
        ], true);
    }


    /**
     * Get current language
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->lang;
    }


    /**
     * Set current language
     *
     * @param $lang
     */
    public function setLanguage($lang)
    {
        if (is_string($lang)) {
            $lang = $this->db->repository(Language::class)->find(['name' => $lang], true);
        }
        $this->lang = $lang;
    }


    /**
     * Translate database record fields
     *
     * @param $table
     * @param $id
     * @return array
     */
    public function record($table, $id)
    {
        /** @var Translation[] $translations */
        $translations = $this->db->repository(Translation::class)->find([
            'language_id' => $this->lang->id,
            'key' => ['like', $table . '-%-' . $id]
        ]);

        $result = [];

        foreach ($translations as $translation) {
            list($table, $field, $id) = explode('-', $translation->key);
            $result[$field] = $translation->text;
        }

        return $result;
    }
}
