<?php

namespace Framework\Translation\Repository;

use Framework\Database\Repository;

class LanguageRepository extends Repository
{
    /**
     * @param $language_id
     * @param array $arguments
     * @return mixed
     */
    public function findTranslations($language_id, $arguments = [])
    {
        $params = [];
        $arguments['language_id'] = $language_id;

        foreach ($arguments as $key => $value) {
            $params[] = "`$key` = :$key";
        }

        $query = "SELECT * FROM translation WHERE " . implode(' AND ', $params);

        return $this->db->fetch($query, $arguments);
    }

}
