<?php

namespace Framework\Translation;

use Exception;

class LocalizedException extends Exception
{
    /**
     * LocalizedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $message = app()->singleton(TranslationService::class)->translate($message);

        parent::__construct($message, $code, $previous);
    }
}
