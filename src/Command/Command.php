<?php

namespace Framework\Command;

abstract class Command
{
    protected $commandService;
    protected $debug_mode = false;


    /**
     * Command constructor.
     *
     * @param CommandService $commandService
     */
    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }


    /**
     * Write string into console if debug mode is enabled
     *
     * @param $string
     */
    public function debug($string)
    {
        if ($this->debug_mode) {
            echo $string;
        }
    }


    /**
     * @return bool
     */
    public function getDebugMode()
    {
        return $this->debug_mode;
    }


    /**
     * @param $mode
     * @return $this
     */
    public function setDebugMode($mode)
    {
        $this->debug_mode = $mode;
        return $this;
    }


    /**
     * If command finished successfully
     *
     * @param null $data
     * @param string $message
     * @return array
     */
    public function success($data = null, $message = '')
    {
        return ['success' => true, 'data' => $data, 'message' => $message];
    }


    /**
     * If command failed
     *
     * @param $message
     * @return array
     */
    public function failure($message)
    {
        if (is_array($message)) {
            $message['success'] = false;
            return $message;
        }
        return ['success' => false, 'message' => $message];
    }


    /**
     * Get command name
     *
     * @return string
     */
    public static function getName(){
        return '';
    }


    /**
     * Get command short description for console
     *
     * @return string
     */
    public static function getNote(){
        return '';
    }


    /**
     * Get command description for console
     *
     * @return string
     */
    public static function getDescription(){
        return '';
    }

}
