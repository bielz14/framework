<?php

namespace Framework\Command\Commands;

use Framework\Command\Command;
use Framework\Command\CommandService;

class Help extends Command
{
    public function run(CommandService $commandService)
    {
        $commands = $commandService->getList();

        foreach ($commands as $command) {
            $name = $command::getName();

            if (!empty($name)) {
                $name = str_pad($name, 30, " ", STR_PAD_RIGHT);
                $this->debug($name . $command::getNote() . PHP_EOL);
            }
        }
    }

}
