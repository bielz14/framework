<?php

namespace Framework\Command;

class CommandService
{
    /**
     * @var string[]
     */
    private $commands = [];


    /**
     * Run command execution
     *
     * @param string $alias
     * @param array $arguments
     * @return array|mixed
     */
    public function run($alias, $arguments = [], $debug_mode = false)
    {
        if (!$this->exists($alias)) {
            return ['success' => false, 'message' => 'Command is not exists'];
        }

        /** @var Command $command */
        $command = app()->make($this->commands[$alias]);
        $command->setDebugMode($debug_mode);

        // Optional arguments, unnamed
        $unnamed = [];
        foreach ($arguments as $key => $value) {
            if (is_numeric($key)) {
                $unnamed[] = $value;
            }
        }

        // Fill command instance parameters from parsed arguments
        foreach (get_object_vars($command) as $property => $value) {
            if (isset($arguments[$property])) {
                $command->{$property} = $arguments[$property];
            } elseif (count($unnamed)) {
                $command->{$property} = array_shift($unnamed);
            }
        }

        try {
            return app()->call($command, 'run', $arguments);
        } catch (\Exception $e) {
            return $command->failure($e->getMessage());
        }
    }


    /**
     * Command is registered
     *
     * @param string $name
     * @return bool
     */
    public function exists($name)
    {
        return array_key_exists($name, $this->commands);
    }


    /**
     * Get commands list in format: alias => classname
     *
     * @return array
     */
    public function getList()
    {
        return $this->commands;
    }


    /**
     * Register command by classname.
     * You can register many commands by calling with array of pairs alias=>classname as alias parameter
     *
     * @param string|array $alias
     * @param string|null $className
     */
    public function register($alias, $className = null)
    {
        if (is_array($alias)) {
            foreach ($alias as $name => $className) {
                $this->commands[$name] = $className;
            }
        } else {
            $this->commands[$alias] = $className;
        }
    }

}
