<?php

namespace Framework\Session;

class SessionService
{
    /**
     * SessionService constructor.
     */
    public function __construct()
    {
        session_name('session');
        session_start();
    }


    /**
     * @param $property
     * @return mixed
     */
    public function &__get($property)
    {
        if (!array_key_exists($property, $_SESSION)) {
            $_SESSION[$property] = null;
        }
        return $_SESSION[$property];
    }


    /**
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $_SESSION[$property] = $value;
    }


    /**
     * Unset all session properties
     */
    public function clear()
    {
        foreach (array_keys($_SESSION) as $key) {
            unset($_SESSION[$key]);
        }
    }

}
