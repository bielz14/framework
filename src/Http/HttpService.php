<?php

namespace Framework\Http;

use Framework\Routing\Route;
use Framework\Routing\RoutingService;

class HttpService
{
    /** @var Request */
    private $request;

    /** @var RoutingService */
    private $routing;

    /** @var Route */
    private $route;


    /**
     * HttpService constructor.
     *
     * @param Request $request
     * @param RoutingService $routing
     */
    public function __construct(Request $request, RoutingService $routing)
    {
        $this->request = $request;
        $this->routing = $routing;
    }


    /**
     * Handle http request
     */
    public function handle()
    {
        $route = $this->getCurrentRoute();

        try {
            $controller = app()->make(app()->getNamespaceByPath('Controller\\' . $route->getControllerName()));
            app()->call($controller, $route->getControllerMethod(), $route->getArguments());
        } catch (NotFoundException $e) {
            $this->pageNotFound($route->getArguments());
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }


    /**
     * Show 404 error page
     * The route /error404 should be defined and
     * handler for this route should be defined in controller too
     *
     * @param null $args
     */
    protected function pageNotFound($args = null)
    {
        $route = $this->routing->match('/error404');
        $controller = app()->make(app()->getNamespaceByPath('Controller\\' . $route->getControllerName()));
        app()->call($controller, $route->getControllerMethod(), $args);
    }


    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }


    /**
     * Redirect to url
     *
     * @param $url
     * @param array $args
     */
    public function redirect($url, $args = [])
    {
        if (strpos($url, 'http') === false) {
            $url = preg_replace('/[\/\\\]{2,}/', '/', $this->getBasePath() . $this->routing->generate($url, $args));
        }
        header('Location: ' . (empty($url) ? '/' : $url));
        exit(0);
    }


    /**
     * Get route for current page
     *
     * @return Route
     */
    public function getCurrentRoute()
    {
        if (!$this->route) {
            $this->route = $this->routing->match($this->request->url);

            if (!$this->route) {
                $this->route = $this->routing->match('error404');
            }
        }
        return $this->route;
    }


    /**
     * Generate url with arguments (project and language for example)
     *
     * @param $link
     * @param array $args
     * @param bool $full
     * @param string $method
     * @return mixed|string
     */
    public function generateUrl($link, $args = [], $full = false, $method = 'GET')
    {
        $path = $this->getBasePath() . $this->routing->generate($link, $args, $method);
        $path = str_replace('//', '/', $path);

        return !$full ? $path : rtrim($this->getRequest()->baseUrl, '/') . '/' . ltrim($path, '/');
    }


    /**
     * Get application root path
     * For example:
     * if server root dir is /var/www/html and application root dir is /var/www/html/application
     * then function returns 'application'
     *
     * @param bool $with_slash
     * @return string
     */
    public function getBasePath($with_slash = false)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, str_replace('/', DIRECTORY_SEPARATOR, realpath(app()->getRootDir())));
        $result = str_replace('\\', DIRECTORY_SEPARATOR, str_replace('/', DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']));
        $result = '/' . trim(str_replace(strtolower($result), '', strtolower($path)), DIRECTORY_SEPARATOR);

        return $with_slash ? ($result == '/' ? $result : $result . '/') : $result;
    }
}
