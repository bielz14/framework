<?php

namespace Framework\Http;

use Framework\Filesystem\FilesystemService;

class Request
{
    const GET  = 'GET';
    const POST = 'POST';
    const PROTOCOL_HTTP  = 'http';
    const PROTOCOL_HTTPS = 'https';

    public $path;
    public $method;
    public $self;
    public $host;
    public $url;
    public $protocol;
    public $baseUrl;
    public $ip;
    public $remoteIp;
    public $postData;
    public $files;
    protected $arguments;


    /**
     * Request constructor.
     */
    public function __construct()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $this->path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        } elseif (isset($_SERVER['PATH_INFO'])) {
            $this->path = $_SERVER['PATH_INFO'];
        }

        if (!empty($_SERVER['HTTPS']) && 0 !== strcasecmp($_SERVER['HTTPS'], 'off')) {
            $this->protocol = self::PROTOCOL_HTTPS;
        } else {
            $this->protocol = self::PROTOCOL_HTTP;
        }

        if (!empty($_SERVER['REMOTE_ADDR'])) {
            $this->remoteIp = $_SERVER['REMOTE_ADDR'];
        }

        $this->host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null;
        $this->baseUrl = $this->protocol . '://' . $this->host;
        $this->method = isset($_SERVER['REQUEST_METHOD']) ? strtoupper($_SERVER['REQUEST_METHOD']) : self::GET;

        $url  = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
        $this->url = $url;

        $name = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : '';
        $this->self = 0 === strncmp($url, $name, strlen($name)) ? $name : '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $this->ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $this->ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }
    }


    /**
     * Get argument from uri (search str)
     *
     * @param $key
     * @return null
     */
    public function getArgument($key)
    {
        if (!$this->arguments) {
            $this->getArguments();
        }
        return isset($this->arguments[$key]) ? $this->arguments[$key] : null;
    }


    /**
     * Get all arguments from uri (search str)
     *
     * @return array
     */
    public function getArguments()
    {
        if (!$this->arguments) {
            $this->arguments = [];

            foreach ($_GET as $key => $value) {
                $this->arguments[$key] = urldecode($value);
            }
        }
        return $this->arguments;
    }


    /**
     * @return array
     */
    public function getPostData()
    {
        if ($this->postData) {
            return $this->postData;
        }

        $this->postData = array();
        $contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : false;

        if ($this->method == 'POST') {
            if ($contentType && (strpos($contentType, 'application/x-www-form-urlencoded') === 0)) {
                parse_str(file_get_contents("php://input"), $this->postData);
            } elseif ($contentType && strpos($contentType, 'multipart/form-data') === 0) {
                if (count($_POST)) {
                    $this->postData = $_POST;
                }
            } elseif ($contentType && strpos($contentType, 'application/json') === 0) {
                $this->postData = (array)json_decode(file_get_contents("php://input"));
            }
        }

        return $this->postData;
    }


    /**
     * @return array
     */
    public function getFiles()
    {
        if ($this->files) {
            return $this->files;
        }

        $this->files = array();

        foreach ($_FILES as $name => $file) {
            if (is_array($file['name'])) {
                foreach ($file['name'] as $index => $filename) {
                    $this->appendFile($index, $filename, $file['tmp_name'][$index], $name);
                }
            } else {
                $this->appendFile($name, $file['name'], $file['tmp_name']);
            }
        }

        return $this->files;
    }


    /**
     * @param $key
     * @param $filename
     * @param $tempname
     * @param null $group
     * @return bool
     */
    protected function appendFile($key, $filename, $tempname, $group = null)
    {
        if (is_array($filename)) {
            foreach ($filename as $index => $name) {
                $this->appendFile($key, $name, $tempname[$index], $group);
            }
            return true;
        }

        if (empty($filename)) {
            $path = '';
        } else {
            $fs = app()->singleton(FilesystemService::class);
            $info = pathinfo($filename);
            $hash = md5($filename . time() . rand(0, 1000)) . '.' . $info['extension'];
            $path = $fs->getStoragePath('temp/' . $hash);
            $fs->makeDir($fs->getStoragePath('temp'));
            move_uploaded_file($tempname, $fs->getPath($path));
        }

        if ($group) {
            if (!array_key_exists($group, $this->files)) {
                $this->files[$group] = [];
            }

            if (array_key_exists($key, $this->files[$group])) {
                if (!is_array($this->files[$group][$key])) {
                    $tmp = $this->files[$group][$key];
                    $this->files[$group][$key] = [];
                    $this->files[$group][$key][] = $tmp;
                }
                $this->files[$group][$key][] = $path;
            } else {
                $this->files[$group][$key] = $path;
            }
        } else {
            if (array_key_exists($key, $this->files)) {
                if (!is_array($this->files[$key])) {
                    $tmp = $this->files[$key];
                    $this->files[$key] = [];
                    $this->files[$key][] = $tmp;
                }
                $this->files[$key][] = $path;
            } else {
                $this->files[$key] = $path;
            }
        }

        return true;
    }
}
