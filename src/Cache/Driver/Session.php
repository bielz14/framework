<?php

namespace Framework\Cache\Driver;

use Framework\Session\SessionService;

class Session
{
    private $session;


    /**
     * Session constructor.
     *
     * @param SessionService $session
     */
    public function __construct(SessionService $session)
    {
        $this->session = $session;

        if (!$session->cache) {
            $session->cache = [];
        }
    }


    /**
     * Is key exists in cache
     *
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        return array_key_exists($key, $this->session->cache) && $this->session->cache[$key] != null;
    }


    /**
     * Get value by key from cache
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        return $this->exists($key) ? $this->session->cache[$key] : $default;
    }


    /**
     * Set value by key
     *
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value)
    {
        $this->session->cache[$key] = $value;
        return true;
    }


    /**
     * Remove key from the cache
     *
     * @param $key
     */
    public function remove($key)
    {
        $this->session->cache[$key] = null;
    }

}
