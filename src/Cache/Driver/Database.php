<?php

namespace Framework\Cache\Driver;

use Framework\Database\DatabaseService;

class Database
{
    private $db;


    /**
     * Database constructor.
     *
     * @param DatabaseService $db
     */
    public function __construct(DatabaseService $db)
    {
        $this->db = $db;
    }


    /**
     * Is key exists in cache
     *
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        $result = $this->db->fetch("select `key`, `value` from `cache` where `key` = :key", [
            'key' => $key
        ], true);

        return !is_null($result);
    }


    /**
     * Get value by key from cache
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        $result = $this->db->fetch("select `id`, `key`, `value` from `cache` where `key` = :key", [
            'key' => $key
        ], true);

        return !is_null($result) ? json_decode($result['value']) : $default;
    }


    /**
     * Set value by key
     *
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value)
    {
        $record = $this->get($key);
        $value = json_encode($value);

        if ($record) {
            $this->db->update("cache", ['value' => $value], ['key' => $key]);
        } else {
            $record = $this->db->insert("cache", [
                'key' => $key,
                'value' => $value
            ]);
        }

        return !empty($record);
    }


    /**
     * Remove key from the cache
     *
     * @param $key
     */
    public function remove($key)
    {
        $this->db->delete("cache", ['key' => $key]);
    }

}
