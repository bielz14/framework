<?php

namespace Framework\Cache;

use Framework\Config\ConfigService;

class CacheService
{
    private $driver;
    private $instance;


    /**
     * CacheService constructor.
     *
     * @param ConfigService $config
     */
    public function __construct(ConfigService $config)
    {
        $this->driver = $config->get('cache.driver', 'session');
        $this->instance = app()->make(__NAMESPACE__ . '\\Driver\\' . ucfirst($this->driver));
    }


    /**
     * @param $key
     * @return mixed
     */
    public function exists($key)
    {
        return $this->instance->exists($key);
    }


    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->instance->get($key, $default);
    }


    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function set($key, $value)
    {
        return $this->instance->set($key, $value);
    }


    /**
     * @param $key
     */
    public function remove($key)
    {
        $this->instance->remove($key);
    }

}
