-- @section statements
-- you can write sql query here

CREATE TABLE `cache` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(255) NOT NULL COMMENT 'Ключ',
  `value` TEXT COMMENT 'Значение',
  PRIMARY KEY (`id`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here


