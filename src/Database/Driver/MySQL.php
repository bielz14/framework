<?php

namespace Framework\Database\Driver;

use Framework\Contracts\DatabaseInterface;
use PDO;

class MySQL implements DatabaseInterface
{
    protected $name;
    protected $host;
    protected $port;
    protected $user;
    protected $password;
    protected $connection;


    /**
     * MySQL constructor.
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->name = $config['name'];
        $this->host = $config['host'];
        $this->user = $config['user'];
        $this->port = $config['port'];
        $this->password = $config['password'];

        $dsn = "mysql:host={$this->host};port={$this->port};dbname={$this->name};charset=utf8";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->connection = new PDO($dsn, $this->user, $this->password, $opt);
    }


    /**
     * @return mixed
     */
    public function getDatabseName()
    {
        return $this->name;
    }


    /**
     * @param $str
     * @return mixed
     */
    public function escapeString($str)
    {
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

        return str_replace($search, $replace, $str);
    }


    /**
     * @param $query
     * @param $params
     * @param bool $single
     * @return array|mixed
     */
    public function fetch($query, $params, $single = false)
    {
        $query_params = [];
        $counter = [];
        $matches = [];
        preg_match_all('~:([a-z_]+)~', $query, $matches);
        if (count($matches) > 1) {
            foreach ($matches[1] as $match) {
                if (!isset($counter[$match])) {
                    $counter[$match] = 0;
                }
                $counter[$match] ++;
                $query_params[$match . $counter[$match]] = array_key_exists($match, $params) ? $params[$match] : null;
            }
        }

        $query = preg_replace_callback('~:([a-z_]+)~', function($matches) use(&$counter) {
            $param = $matches[0] . $counter[$matches[1]];
            $counter[$matches[1]]--;
            return $param;
        }, $query);

        $stmt = $this->connection->prepare($query);
        $stmt->execute($query_params);
        if ($single) {
            return $stmt->fetch();
        }
        return $stmt->fetchAll();
    }


    /**
     * Insert record with parameters
     *
     * @param $table
     * @param $params
     * @return string
     */
    public function insert($table, $params)
    {
        $columns = [];
        $keys = [];
        foreach ($params as $key => $value) {
            $keys[] = "`$key`";
            $columns[] = ':' . $key;
        }
        $stmt = $this->connection->prepare("insert into {$table}(" . implode(',', $keys) . ") values (" . implode(',', $columns) . ")");
        $stmt->execute($params);
        return $this->connection->lastInsertId();
    }


    /**
     * Update records with parameters
     *
     * @param $table
     * @param $params
     * @param $where
     * @return $this
     */
    public function update($table, $params, $where)
    {
        $values = array();
        foreach ($params as $field => $value) {
            $values[] = "`{$field}`=:{$field}";
        }
        $query = "update {$table} set " . implode(',', $values);

        if (isset($where) && count($where)) {
            $where_values = array();
            foreach ($where as $field => $value) {
                if (is_null($values)) {
                    $where_values[] = "`{$field}` is null";
                } else {
                    $where_values[] = "`{$field}`=:{$field}";
                }
            }
            $query .= " where " . implode(' and ', $where_values);
        }
        $stmt = $this->connection->prepare($query);
        $stmt->execute(array_merge($params, $where));

        return $this;
    }


    /**
     * Delete records from table
     *
     * @param $table
     * @param $where
     * @return $this
     */
    public function delete($table, $where)
    {
        $query = "delete from {$table}";

        if (isset($where) && count($where)) {
            $where_values = array();
            foreach ($where as $field => $value) {
                if (is_null($value)) {
                    $where_values[] = "{$field} is null";
                } else {
                    $where_values[] = "{$field}=:{$field}";
                }
            }
            $query .= " where " . implode(' and ', $where_values);
        }
        $stmt = $this->connection->prepare($query);
        $stmt->execute($where);
        return $this;
    }


    /**
     * Execute query with parameters
     *
     * @param $query
     * @param $params
     * @return $this
     */
    public function execute($query, $params)
    {
        $stmt = $this->connection->prepare($query);
        $stmt->execute($params);
        return $this;
    }


    /**
     * Begin transaction
     */
    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }


    /**
     * Commit changes
     */
    public function commit()
    {
        $this->connection->commit();
    }


    /**
     * Rollback changes
     */
    public function rollback()
    {
        $this->connection->rollback();
    }
}
