<?php

namespace Framework\Database;

use Framework\Config\ConfigService;
use Framework\Contracts\DatabaseInterface;

class DatabaseService implements DatabaseInterface
{
    /**
     * @var DatabaseInterface[]
     */
    protected $connections = [];

    /**
     * Default connection name
     * @var string
     */
    protected $default;

    /**
     * @var Repository[]
     */
    protected $repositories = [];

    protected $configService;


    /**
     * DatabaseService constructor.
     *
     * @param ConfigService $configService
     */
    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
        $this->default = $this->configService->get('database.default', 'default');
    }


    /**
     * Use connection as default
     *
     * For use second connection you must add connection parameters into config/database.php or append new connection config
     * to DatabaseService by execution of method addConnection('new_conn').
     * Then you can use new connection: app()->singleton(DatabaseService::class)->useConnection('new_conn')->fetch('...');
     *
     * @param $name
     * @return $this
     */
    public function useConnection($name)
    {
        $this->default = $name;
        return $this;
    }


    /**
     * Append new connection to service
     *
     * @param $name
     * @param $config
     * @return $this
     */
    public function addConnection($name, $config)
    {
        $this->configService->set('database.connections.' . $name, $config);
        return $this;
    }


    /**
     * Get connection driver by config name or default
     *
     * @param null $name
     * @return DatabaseInterface
     * @throws \Exception
     */
    protected function connection($name = null)
    {
        if (empty($name)) {
            $name = $this->default;
        }

        if (!array_key_exists($name, $this->connections)) {
            $config = $this->configService->get('database.connections.' . $name);

            if (empty($config)) {
                throw new \Exception('Database connection is not configured');
            }

            $this->connections[$name] = app()->make(__NAMESPACE__ . '\\Driver\\' . $config['driver'], compact('config'));
        }
        return $this->connections[$name];
    }


    /**
     * @param $str
     * @return mixed
     */
    public function escapeString($str)
    {
        return $this->connection()->escapeString($str);
    }


    /**
     * @param string $entityClass
     * @return Repository
     */
    public function repository($entityClass)
    {
        if (!array_key_exists($entityClass, $this->repositories)) {
            $chain = explode('\\', str_replace('\Entity\\', '\Repository\\', $entityClass));
            $class = array_pop($chain);
            $chain[] = $class . 'Repository';
            $this->repositories[$entityClass] = app()->singleton(implode('\\', $chain));
        }
        return $this->repositories[$entityClass];
    }


    /**
     * @param $query
     * @param array $params
     * @param bool $single
     * @return mixed
     */
    public function fetch($query, $params = [], $single = false)
    {
        return $this->connection()->fetch($query, $params, $single);
    }


    /**
     * @param $query
     * @param array $params
     * @param $field
     * @return array
     */
    public function map($query, $params = [], $field)
    {
        $rows = $this->connection()->fetch($query, $params);
        $result = [];

        foreach ($rows as $row) {
            $result[$row[$field]] = $row;
        }

        return $result;
    }


    /**
     * @param $table
     * @param $params
     * @return mixed
     */
    public function insert($table, $params)
    {
        return $this->connection()->insert($table, $params);
    }


    /**
     * @param $table
     * @param $params
     * @param $where
     */
    public function update($table, $params, $where)
    {
        $this->connection()->update($table, $params, $where);
    }


    /**
     * @param $table
     * @param $where
     */
    public function delete($table, $where)
    {
        $this->connection()->delete($table, $where);
    }


    /**
     * @param $query
     * @param array $params
     * @return mixed
     */
    public function execute($query, $params = [])
    {
        return $this->connection()->execute($query, $params);
    }


    public function beginTransaction()
    {
        $this->connection()->beginTransaction();
    }


    public function commit()
    {
        $this->connection()->commit();
    }


    public function rollback()
    {
        $this->connection()->rollback();
    }

}
