<?php

namespace Framework\Database;

abstract class Repository
{
    protected $db;
    protected $entity;
    protected $table;


    /**
     * Repository constructor.
     *
     * @param DatabaseService $db
     */
    public function __construct(DatabaseService $db)
    {
        $this->db = $db;
    }


    /**
     * @return DatabaseService
     */
    public function getDatabase()
    {
        return $this->db;
    }


    /**
     * Find Entities or db records
     *
     * @param array $params
     * @param bool $single
     * @param string|array|null $orderBy
     * @param bool $getEntities
     * @return Entity[]|Entity|array|null
     */
    public function find($params = [], $single = false, $orderBy = null, $getEntities = true)
    {
        $query = "select * from " . $this->getTableName() . $this->getWhereString($params) . $this->getOrderByString($orderBy);
        $records = $this->db->fetch($query, $params, $single);

        if (!$getEntities) {
            return $records;
        }

        if ($single) {
            return !$records ? null : $this->entity($records);
        }

        if (!$records || !count($records)) {
            return [];
        }
        return $this->entities($records);
    }


    /**
     * Find Entity or db record by id
     *
     * @param $id
     * @return Entity|null
     */
    public function findById($id)
    {
        return empty($id) ? null : $this->find(['id' => $id], true);
    }


    /**
     * Generate ORDER BY string for query
     *
     * @param array|string $orderBy
     * @return string
     */
    protected function getOrderByString($orderBy)
    {
        if (is_array($orderBy)) {
            $order_result = [];

            foreach ($orderBy as $order_field => $order_dir) {
                $order_result[] = "`$order_field` $order_dir";
            }

            return ' ORDER BY ' . implode(', ', $order_result);
        }

        if (!empty($orderBy)) {
            return ' ORDER BY `' . $orderBy . '` ASC';
        }

        return ' ORDER BY `id` ASC';
    }


    /**
     * Generate WHERE string by query parameters
     *
     * @param $params
     * @return string
     */
    protected function getWhereString(&$params)
    {
        if (!count($params)) {
            return '';
        }

        $where = [];

        foreach ($params as $field => $value) {
            $operator = '=';

            if (!is_array($value)) {
                $where[] = "`{$field}` {$operator} :{$field}";
                continue;
            }

            $operator = array_shift($value);
            $params[$field] = array_shift($value);

            if ($operator == 'in') {
                $where[] = "`{$field}` {$operator} (:{$field})";
            } elseif ($operator == 'like') {
                $where[] = "`{$field}` {$operator} :{$field} ESCAPE '!'";
            }
        }
        return ' WHERE ' . implode(' AND ', $where);
    }


    /**
     * @param array $params
     * @param int $parent_id
     * @param null $map
     * @return array
     */
    public function children($params = [], $parent_id = 0, $map = null)
    {
        if (is_null($parent_id)) {
            $parent_id = 0;
        }

        $result = [];

        if (is_null($map)) {
            $map = [];
            $items = $this->find($params);

            foreach ($items as $index => $item) {
                if (is_null($item->parent_id)) {
                    $item->parent_id = 0;
                }

                if (!array_key_exists($item->parent_id, $map)) {
                    $map[$item->parent_id] = [];
                }

                $map[$item->parent_id][] = &$items[$index];
            }
        }

        if (array_key_exists($parent_id, $map)) {
            foreach ($map[$parent_id] as $item) {
                $result[] = $item;
                $result = array_merge($result, $this->children(null, $item->id, $map));
            }
        }

        return $result;
    }


    /**
     * @param array $params
     * @param int $parent_id
     * @return array
     */
    public function tree($params = [], $parent_id = 0)
    {
        if (is_null($parent_id)) {
            $parent_id = 0;
        }

        $items = $this->find($params);
        $items_map = [];

        foreach ($items as $index => $item) {
            if (is_null($item->parent_id)) {
                $item->parent_id = 0;
            }

            if (!array_key_exists($item->parent_id, $items_map)) {
                $items_map[$item->parent_id] = [];
            }

            $items_map[$item->parent_id][] = &$items[$index];
        }

        return $this->createTree($items_map, $items_map[$parent_id]);
    }


    /**
     * @param $list
     * @param $parent
     * @return array
     */
    private function createTree(&$list, $parent){
        $tree = [];
        foreach ($parent as $k => $child){
            if(isset($list[$child->id])){
                $child->children = $this->createTree($list, $list[$child->id]);
            }
            $tree[] = $child;
        }
        return $tree;
    }


    /**
     * Create new Entity or from existing db record
     *
     * @param array $params
     * @return Entity
     */
    public function entity($params = [])
    {
        return app()->make($this->getEntityClass(), ['data' => $params]);
    }


    /**
     * Create Entities from array of records
     *
     * @param array $records
     * @return array
     */
    public function entities($records)
    {
        $result = [];
        foreach ($records as $record) {
            $result[] = $this->entity($record);
        }
        return $result;
    }


    /**
     * Get Entity class by repository class name
     *
     * @return string
     */
    public function getEntityClass()
    {
        if (isset($this->entity)) {
            return $this->entity;
        }

        $chain = explode('\\', str_replace('\Repository\\','\Entity\\', get_class($this)));
        $entity = array_pop($chain);
        $chain[] = substr($entity, 0, strpos($entity, 'Repository'));
        $this->entity = implode('\\', $chain);
        return $this->entity;
    }


    /**
     * Get table name by repository class name
     *
     * @return string
     */
    public function getTableName()
    {
        if (isset($this->table)) {
            return $this->table;
        }

        $chain = explode('\\', get_class($this));
        $this->table = array_pop($chain);
        $this->table = lcfirst(substr($this->table, 0, strpos($this->table, 'Repository')));

        $this->table = preg_replace_callback('/([A-Z])/', function(array $match) {
            return '_' . strtolower($match[1]);
        }, $this->table);

        return $this->table;
    }


    /**
     * Create record in 'entity1_entity2' table with entity1_id and entity2_id
     *
     * @param Entity $entity1
     * @param Entity $entity2
     * @param array $params
     */
    public function link(Entity $entity1, Entity $entity2, $params = [])
    {
        $entityTable1 = $entity1->getTableName();
        $entityTable2 = $entity2->getTableName();

        $tables = [$entityTable1, $entityTable2];
        sort($tables);
        $table = implode('_', $tables);

        $params[$entityTable1 . '_id'] = $entity1->id;
        $params[$entityTable2 . '_id'] = $entity2->id;

        $delete_params = [];
        $delete_params[$entityTable1 . '_id'] = $entity1->id;
        $delete_params[$entityTable2 . '_id'] = $entity2->id;

        $this->db->delete($table, $delete_params);
        $this->db->insert($table, $params);
    }


    /**
     * Remove all rows from 'entity1_entity2' table with entity1_id and entity2_id
     *
     * @param Entity $entity1
     * @param Entity $entity2
     */
    public function unlink(Entity $entity1, Entity $entity2)
    {
        $entityTable1 = $entity1->getTableName();
        $entityTable2 = $entity2->getTableName();

        $tables = [$entityTable1, $entityTable2];
        sort($tables);
        $table = implode('_', $tables);

        $params = [];
        $params[$entityTable1 . '_id'] = $entity1->id;
        $params[$entityTable2 . '_id'] = $entity2->id;

        $this->db->delete($table, $params);
    }


    /**
     * Unlink all entities by class name
     *
     * @param Entity $entity
     * @param string $class
     * @return $this
     */
    public function unlinkAll(Entity $entity, string $class)
    {
        $entityTable1 = $entity->getTableName();

        $entityTable2 = explode('\\', $class);
        $entityTable2 = array_pop($entityTable2);
        $entityTable2 = preg_replace_callback('/([A-Z])/', function(array $match) {
            return '_' . strtolower($match[1]);
        }, lcfirst($entityTable2));

        $tables = [$entityTable1, $entityTable2];
        sort($tables);
        $table = implode('_', $tables);

        $params = [];
        $params[$entityTable1 . '_id'] = $entity->id;

        $this->db->delete($table, $params);

        return $this;
    }


    /**
     * Return related entities by class name
     *
     * @param Entity $entity
     * @param string $class
     * @return array
     */
    public function getRelations(Entity $entity, string $class)
    {
        $entityTable1 = $entity->getTableName();

        $entityTable2 = explode('\\', $class);
        $entityTable2 = array_pop($entityTable2);
        $entityTable2 = preg_replace_callback('/([A-Z])/', function(array $match) {
            return '_' . strtolower($match[1]);
        }, lcfirst($entityTable2));

        $tables = [$entityTable1, $entityTable2];
        sort($tables);
        $table = implode('_', $tables);

        $query = "select t.* from {$table} l join {$entityTable2} t on l.{$entityTable2}_id = t.id where l.{$entityTable1}_id = :id";
        $rows = $this->db->fetch($query, ['id' => $entity->id]);

        return $this->db->repository($class)->entities($rows);
    }


    /**
     * Save entity changes in database
     *
     * @param Entity $entity
     * @return Entity
     */
    public function save(Entity $entity)
    {
        $entity->checkRequiredProperties(true);
        $changes = $entity->getChanges();

        if ($entity->isNew()) {
            $entity->id = $this->db->insert($entity->getTableName(), $changes);
        } else {
            if (array_key_exists('id', $changes)) {
                unset($changes['id']);
            }
            $this->db->update($entity->getTableName(), $changes, ['id' => $entity->id]);
        }
        return $entity;
    }


    /**
     * Remove entity from database and all links
     *
     * @param Entity $entity
     */
    public function remove(Entity &$entity)
    {
        $table = $entity->getTableName();
        $this->db->delete($table, ['id' => $entity->id]);

        $params = [];
        $params[$table . '_id'] = $entity->id;

        foreach ($entity->getLinks() as $link) {
            $tables = [$table, $link];
            sort($tables);
            $this->db->delete(implode('_', $tables), $params);
        }

        $entity->id = null;
    }
}
