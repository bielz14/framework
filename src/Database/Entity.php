<?php

namespace Framework\Database;

class Entity
{
    /**
     * @var int $id
     */
    public $id;

    protected $prefix = '';
    protected $links = [];
    protected $table;
    protected $properties;
    protected $initial = [];
    protected $db;


    /**
     * Entity constructor.
     *
     * @param DatabaseService $db
     * @param array $data
     */
    public function __construct(DatabaseService $db, $data = [])
    {
        $this->db = $db;

        if (!empty($data)) {
            if (is_object($data)) {
                $data = (array)$data;
            }

            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
            $this->initial = $data;
        }
    }


    /**
     * Get array of entity properties
     *
     * @return array
     */
    public function asArray() {
        $result = [];
        foreach ($this->getProperties() as $name => $params) {
            $result[$name] = $this->{$name};
        }
        return $result;
    }


    /**
     * Get parameters of entity properties
     *
     * @return array
     */
    public function getProperties()
    {
        if (isset($this->properties)) {
            return $this->properties;
        }

        $class = new \ReflectionClass(get_class($this));
        $this->properties = [];

        foreach ($class->getProperties() as $property) {
            if ($property->isPublic() && !$property->isStatic()) {
                $rows = explode(PHP_EOL, $property->getDocComment());
                $params = ['name' => $property->getName(), 'type' => 'string', 'required' => false];

                foreach ($rows as $row) {
                    if (preg_match('~@([a-z]+)[\s]?([a-z]+)?~', $row, $matches) && count($matches) == 2) {
                        if ($matches[1] == 'var') {
                            $params['type'] = $matches[2];
                        } elseif ($matches[1] == 'required') {
                            $params['required'] = true;
                        }
                    }
                }
                $this->properties[$params['name']] = $params;
            }
        }
        return $this->properties;
    }


    /**
     * Get table name by entity class name
     *
     * @return string
     */
    public function getTableName()
    {
        if (isset($this->table)) {
            return $this->table;
        }

        $chain = explode('\\', get_class($this));
        $this->table = lcfirst(array_pop($chain));

        $this->table = preg_replace_callback('/([A-Z])/', function(array $match) {
            return '_' . strtolower($match[1]);
        }, $this->table);

        return $this->table;
    }


    /**
     * Return TRUE if entity is not exported to database
     *
     * @return bool
     */
    public function isNew()
    {
        return empty($this->id);
    }


    /**
     * Save entity changes to database
     *
     * @return Entity
     */
    public function save()
    {
        return $this->getRepository()->save($this);
    }


    /**
     * Delete entity from database
     */
    public function remove()
    {
        $this->getRepository()->remove($this);
    }


    /**
     * @return array
     */
    public function getChanges()
    {
        $result = [];

        foreach ($this->getProperties() as $name => $params) {
            $result[$name] = $this->{$name};
        }
        return $result;
    }


    /**
     * @param null $class
     * @return Repository
     */
    public function getRepository($class = null)
    {
        return $this->db->repository(empty($class) ? get_class($this) : $class);
    }


    /**
     * Check the required properties is not empty
     *
     * @param bool $exception
     * @return bool
     * @throws \Exception
     */
    public function checkRequiredProperties($exception = false)
    {
        foreach ($this->getProperties() as $name => $params) {
            if ($params['required'] && empty($this->{$name})) {
                if ($exception) {
                    throw new \Exception("Поле $name обязательно для заполнения");
                }
                return false;
            }
        }
        return true;
    }


    /**
     * @param Entity $entity
     * @param array $params
     */
    public function link(Entity $entity, $params = [])
    {
        $this->getRepository()->link($this, $entity, $params);
    }


    /**
     * @param Entity $entity
     */
    public function unlink(Entity $entity)
    {
        $this->getRepository()->unlink($this, $entity);
    }


    /**
     * @param $class
     */
    public function unlinkAll($class)
    {
        $this->getRepository()->unlinkAll($this, $class);
    }


    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }
}
