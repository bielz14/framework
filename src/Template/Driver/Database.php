<?php

namespace Framework\Template\Driver;

use Framework\Database\DatabaseService;
use Framework\Template\Entity\View;

class Database
{
    private $db;


    /**
     * Database constructor.
     *
     * @param DatabaseService $db
     */
    public function __construct(DatabaseService $db)
    {
        $this->db = $db;
    }


    /**
     * Is View exists
     *
     * @param $name
     * @return bool
     */
    public function exists($name)
    {
        $result = $this->db->fetch("select id from `view` where `name` = :name", [
            'name' => $name
        ], true);

        return !empty($result);
    }


    /**
     * Get View entity by name
     *
     * @param $name
     * @return array|\Framework\Database\Entity|\Framework\Database\Entity[]|null
     */
    public function get($name)
    {
        return $this->db->repository(View::class)->find(['name' => $name], true);
    }


    /**
     * Set View content by name
     *
     * @param $name
     * @param $content
     */
    public function set($name, $content)
    {
        /** @var View $view */
        $view = $this->db->repository(View::class)->find(['name' => $name], true);
        $view->content = $content;
        $view->save();
    }


    /**
     * Remove View
     *
     * @param $name
     */
    public function remove($name)
    {
        $this->db->repository(View::class)->find(['name' => $name], true)->remove();
    }

}
