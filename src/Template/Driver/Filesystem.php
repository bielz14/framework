<?php

namespace Framework\Template\Driver;

use Framework\Filesystem\FilesystemService;
use Framework\Template\Entity\View;

class Filesystem
{
    private $fs;
    private $path;


    /**
     * Filesystem constructor.
     *
     * @param FilesystemService $fs
     */
    public function __construct(FilesystemService $fs)
    {
        $this->fs = $fs;
        $this->path = $fs->getResourcePath('view');
    }


    /**
     * Is View exists
     *
     * @param $name
     * @return bool
     */
    public function exists($name)
    {
        return $this->fs->exists($this->path . '/' . $name . '.phtml');
    }


    /**
     * Get View entity by name
     *
     * @param $name
     * @return View
     */
    public function get($name)
    {
        $view = app()->make(View::class);
        $content = $this->fs->getContents($this->path . '/' . $name . '.phtml');
        $view->setContent($content);
        $view->name = $name;

        return $view;
    }


    /**
     * Set View content by name
     *
     * @param $name
     * @param $content
     */
    public function set($name, $content)
    {
        $this->fs->putContents($this->path . '/' . $name . '.phtml', $content);
    }


    /**
     * Remove View
     *
     * @param $name
     */
    public function remove($name)
    {
        $this->fs->delete($this->path . '/' . $name . '.phtml');
    }

}
