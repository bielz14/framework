<?php

namespace Framework\Template;

use Framework\Config\ConfigService;

class TemplateService
{
    protected $template;
    protected $driver;


    /**
     * TemplateService constructor.
     *
     * @param ConfigService $config
     */
    public function __construct(ConfigService $config)
    {
        $driver = $config->get('template.driver', 'filesystem');
        $this->driver = app()->make(__NAMESPACE__ . '\\Driver\\' . ucfirst($driver));
    }


    /**
     * @param $class
     * @return object
     */
    public function setTemplate($class)
    {
        $this->template = app()->make($class);
        return $this->template;
    }


    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }


    /**
     * @param $name
     * @return mixed
     */
    public function getView($name)
    {
        return $this->driver->get($name);
    }


    /**
     * @param $name
     * @param $content
     * @return mixed
     */
    public function setView($name, $content)
    {
        return $this->driver->set($name, $content);
    }
}
