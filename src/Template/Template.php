<?php

namespace Framework\Template;

use Framework\Http\HttpService;
use Framework\Manifest\ManifestService;
use Framework\Template\Entity\View;

abstract class Template
{
    public $sections = [];
    protected $rt;
    protected $ts;
    protected $ms;
    protected $name;
    protected $globals = [];
    protected $directives = [];


    /**
     * Template constructor.
     *
     * @param HttpService $http
     * @param TemplateService $ts
     * @param ManifestService $ms
     */
    public function __construct(HttpService $http, TemplateService $ts, ManifestService $ms)
    {
        $this->http = $http;
        $this->ts = $ts;
        $this->ms = $ms;

        $this->set('template', $this);
        $this->init();
    }


    /**
     * Register directives
     */
    public function init()
    {
        $this->register('url', function($expression, View $view) {
            return '<?php echo $template->url(' . $view->prepare($expression) . '); ?>';
        });

        $this->register('if', function($expression) {
            return '<?php if (' . $expression . ') : ?>';
        });

        $this->register('elseif', function($expression) {
            return '<?php elseif (' . $expression . ') : ?>';
        });

        $this->register('else', function() {
            return '<?php else : ?>';
        });

        $this->register('endif', function() {
            return '<?php endif; ?>';
        });

        $this->register('widget', function($expression) {
            $args = "";
            if (($pos = strpos($expression, ",")) !== false) {
                $args = trim(substr($expression, $pos + 1));
            }
            return '<?php $template->widget(' . $expression . ')->display(' . $args . '); ?>';
        });

        $this->register('foreach', function($expression, View $view) {
            return '<?php foreach(' . $view->prepare($expression) . ') : ?>';
        });

        $this->register('endforeach', function() {
            return '<?php endforeach; ?>';
        });

        $this->register('for', function($expression, View $view) {
            return '<?php for(' . $view->prepare($expression) . ') : ?>';
        });

        $this->register('endfor', function() {
            return '<?php endfor; ?>';
        });

        $this->register('php', function() {
            return '<?php ';
        });

        $this->register('endphp', function() {
            return ' ?>';
        });

        $this->register('yield', function($expression) {
            return '#--section--' . trim($expression, '"\'') . '--#';
        });

        $this->register('include', function($expression) {
            $view_name = $expression;
            $args = '[]';

            if (($pos = strpos($expression, ",")) !== false) {
                $view_name = trim(substr($expression, 0, $pos));
                $args = trim(substr($expression, $pos + 1));

                if ($args[0] != '[') {
                    $args = "[{$args}]";
                }
            }
            return '<?php echo $template->view(' . $view_name . ', array_merge(' . $args . ', $view->getArguments()))->render(); ?>';
        });

        $this->register('asset', function($expression) {
            return '<?php echo $template->asset(' . $expression . '); ?>';
        });

        $this->register('var', function($expression) {
            if (strpos($expression, ',') !== false) {
                if (preg_match('/\'([A-Za-z_0-9]+)\',(.+)/', $expression, $matches)) {
                    return '<?php $' . $matches[1] . '=' . $matches[2] . '; ?>';
                }
                return '';
            }

            if (preg_match('/\'([A-Za-z_0-9]+)\'/', $expression, $matches)) {
                return '<?php echo $' . $matches[1] . '; ?>';
            }
            return '';
        });

        $this->register('extend', function($expression, View $view) {
            $name = trim($expression, "'");
            $layout = $view->getTemplate()->view($name);
            $view->setLayout($layout);
            return '<?php echo $template->view(' . $expression . ', $view->getArguments())->render(); ?>';
        });

        $this->register('section', function($expression) {
            return '<?php $view->startSection(' . $expression . '); ?>';
        });

        $this->register('endsection', function() {
            return '<?php $view->endSection(); ?>';
        });
    }


    /**
     * @param View $view
     * @param $expression
     * @return mixed
     */
    public function args(View $view, $expression)
    {
        $args = $view->prepare('[' . $expression . ']');
        extract($view->getArguments());
        return eval('return ' . $args . ';');
    }


    /**
     * Execute directive handler
     *
     * @param $directive
     * @param $view
     * @param $args
     * @return mixed
     */
    public function exec($directive, $view, $args)
    {
        return $this->directives[$directive][1]($args, $view);
    }


    /**
     * Is directive exists
     *
     * @param $directive
     * @return bool
     */
    public function has($directive)
    {
        return array_key_exists($directive, $this->directives);
    }


    /**
     * Register directive
     *
     * @param $name
     * @param $func
     */
    public function register($name, $func)
    {
        $this->directives[$name] = ['@' . $name, $func];
    }


    /**
     * Create view and display
     *
     * @param $view
     * @param array $arguments
     */
    public function display($view, $arguments = [])
    {
        $this->view($view, $arguments)->display();
    }


    /**
     * Parse and show content
     *
     * @param $content
     * @param array $arguments
     */
    public function displayContent($content, $arguments = [])
    {
        app()->make(View::class)
            ->setArguments($arguments)
            ->setContent($content ?? '')
            ->display();
    }


    /**
     * Create View instance
     *
     * @param $view
     * @param array $arguments
     * @return mixed
     */
    public function view($view, $arguments = [])
    {
        return $this->ts->getView($view)->setArguments($arguments);
    }


    /**
     * Get Widget entity by name
     *
     * @param $name
     * @return bool|mixed
     */
    public function widget($name)
    {
        $class = app()->getNamespaceByPath('Widget\\' . ucfirst($name));
        return app()->singleton($class);
    }


    /**
     * Define directive for use in View content
     *
     * @param $directive
     * @param $callback
     */
    public function directive($directive, $callback)
    {
        $this->directives[$directive] = $callback;
    }


    /**
     * Set global variable
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        $this->globals[$key] = $value;
        return $this;
    }


    /**
     * Get global variable value for all levels of view
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        return array_key_exists($key, $this->globals) ? $this->globals[$key] : $default;
    }


    /**
     * Get real resource name and path by ManifestService
     *
     * @param $path
     * @param bool $full
     * @return string
     */
    public function asset($path, $full = false)
    {
        $path = $this->ms->path($path);
        if ($full) {
            $path = rtrim($this->http->getRequest()->baseUrl, '/') . '/' . ltrim($path, '/');
        }
        return $path;
    }


    /**
     * @param $url
     * @param string $append
     * @param bool $full
     * @return mixed|string
     */
    public function url($url, $append = '', $full = false)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
            return $url;
        }

        $url = $this->http->generateUrl($url, [], $full) ?? '/';

        if (!empty($append) && substr($append, 0, 1) == '/') {
            $url = rtrim($url, '/');
        }

        return $url . $append;
    }


    /**
     * Parse view content
     *
     * @param $content
     * @return array|string
     */
    public function parse(&$content)
    {
        if (empty($content)) {
            return '';
        }

        $queue = [];
        $queue_length = 0;
        $len = strlen($content);
        $status = 0;
        $tag = '';
        $bracket_depth = 0;
        $result = '';

        for ($i = 0; $i <= $len; $i++) {
            if ($i == $len) {
                if ($status == 0) {
                    break;
                }
                $content .= "\n";
            }

            if ($status == 0) {                 // begining
                if ($content[$i] == '@') {
                    $status = 1;
                    $tag = '';
                } else {
                    $result .= $content[$i];
                }
            } elseif ($status == 1) {           // if tag found
                if (!preg_match('~^[a-zA-Z0-9_]+$~', $content[$i])) {
                    if ($this->has($tag)) {
                        $status = 2;
                        $i--;

                        if ($result != '') {
                            $queue[] = ['tag' => 'text', 'content' => $result];
                            $queue_length ++;
                            $result = '';
                        }

                        $queue[] = ['tag' => $tag, 'args' => ''];
                        $queue_length++;
                    } else {
                        $status = 0;
                        $result .= '@' . $tag . $content[$i];
                    }
                } else {
                    $tag .= $content[$i];
                }
            } elseif ($status == 2) {           // wait params
                if ($content[$i] == '(') {
                    $status = 3;
                    $bracket_depth = 1;
                } elseif ($content[$i] != ' ') {
                    $status = 0;
                    $i--;
                }
            } elseif ($status == 3) {           // get params
                if ($content[$i] == '(') {
                    $bracket_depth ++;
                } elseif ($content[$i] == ')') {
                    $bracket_depth --;
                    if ($bracket_depth == 0) {
                        $status = 0;
                        continue;
                    }
                }
                $queue[$queue_length-1]['args'] .= $content[$i];
            } else {
                if ($content[$i] == "\r" || $content[$i] == "\n" || ($i > 0 && $content[$i] == " " && $content[$i-1] == " ")) {
                    $result .= $content[$i];
                }
            }
        }

        if ($result != '') {
            $queue[] = ['tag' => 'text', 'content' => $result];
        }

        return $queue;
    }
}
