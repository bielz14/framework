<?php

namespace Framework\Template\Entity;

use Framework\Database\DatabaseService;
use Framework\Template\Template;
use Framework\Template\TemplateService;
use Framework\Translation\Entity\LocalizedEntity;
use Framework\Translation\TranslationService;

class View extends LocalizedEntity
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $content;

    /**
     * @var View
     */
    protected $layout;

    /**
     * @var Template
     */
    protected $template;

    /**
     * @var array
     */
    protected $arguments = [];

    private $sections_stack = [];


    /**
     * View constructor.
     *
     * @param DatabaseService $db
     * @param TranslationService $tr
     * @param TemplateService $ts
     * @param array $data
     */
    public function __construct(DatabaseService $db, TranslationService $tr, TemplateService $ts, array $data = [])
    {
        parent::__construct($db, $tr, $data);

        $this->template = $ts->getTemplate();
    }


    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        $this->arguments[$key] = $value;
        return $this;
    }


    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->arguments[$key];
    }


    /**
     * @param $args
     * @return $this
     */
    public function setArguments($args)
    {
        $this->arguments = $args;
        return $this;
    }


    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }


    /**
     * @param $name
     */
    public function startSection($name)
    {
        if ($this->layout) {
            $this->layout->startSection($name);
        } else {
            $this->sections_stack[] = $name;
            ob_start();
        }
    }


    public function endSection()
    {
        if ($this->layout) {
            $this->layout->endSection();
        } else {
            $name = array_pop($this->sections_stack);
            $this->template->sections[$name] = ob_get_clean();
        }
    }


    /**
     * @param $name
     * @return mixed|string
     */
    public function getSection($name)
    {
        return !empty($this->template->sections[$name]) ? $this->handlePlaceholders($this->template->sections[$name]) : '';
    }


    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }


    /**
     * @param $view
     * @return $this
     */
    public function setLayout($view)
    {
        $this->layout = $view;
        return $this;
    }


    /*
     * @return View|null
     */
    public function getLayout()
    {
        return $this->layout;
    }


    /**
     * @return string
     */
    public function getContent()
    {
        return $this->prepare($this->content);
    }


    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        $level = ob_get_level();
        ob_start();
        extract($this->arguments);
        $view = $this;
        $template = $this->template;

        try {
            eval('?>' . $this->getContent() . '<?php ');
        } catch (\Exception $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }
            throw $e;
        }
        return ob_get_clean();
    }


    public function display()
    {
        echo $this->html();
    }


    /**
     * @return mixed
     */
    public function html()
    {
        return $this->handlePlaceholders($this->render());
    }


    /**
     * @param $content
     * @return mixed
     */
    public function handlePlaceholders($content)
    {
        return preg_replace_callback('~#--([a-z]+)--([a-z_]+)--#~', function($matches) {
            return call_user_func(array($this, 'get' . ucfirst($matches[1])), $matches[2]);
        }, $content);
    }


    /**
     * @param $content
     * @return mixed
     */
    public function handleVariables($content)
    {
        return preg_replace_callback('~{{(.+?)}}([\r\n]|[\n])?~', function($matches) {
            if (!is_string($matches[1])) {
                return '';
            }
            $count = count($matches);
            if ($count == 2) {
                return '<?php echo ' . trim($matches[1]) . '; ?>';
            } elseif ($count == 3) {
                return '<?php echo ' . trim($matches[1]) . ' . "' . $matches[2] . '"; ?>';
            }
            return '';
        }, $content);
    }


    /**
     * @param $content
     * @return mixed
     */
    public function handleComments($content)
    {
        return preg_replace('~{{--(.+?)--}}?~', '', $content);
    }


    /**
     * @param $content
     * @return string
     */
    public function prepare($content)
    {
        if (empty($content)) {
            return '';
        }

        $content = $this->handleComments($content);
        $content = $this->handleVariables($content);
        $parsed = $this->template->parse($content);

        $result = '';
        for ($i = 0; $i < count($parsed); $i++) {
            if ($parsed[$i]['tag'] == 'text') {
                $result .= $parsed[$i]['content'];
            } else {
                $result .= $this->template->exec($parsed[$i]['tag'], $this, $parsed[$i]['args']);
            }
        }
        return $result;
    }

}
