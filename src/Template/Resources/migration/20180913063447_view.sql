-- @section statements
-- you can write sql query here

CREATE TABLE `view` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NULL,
  `content` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `VIEW_NAME_INDEX` (`name`) USING BTREE
)
COLLATE='utf8_bin'
ENGINE=InnoDB;

-- @section rollback
-- you can write sql query for rollback migration here