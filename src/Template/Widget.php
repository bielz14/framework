<?php

namespace Framework\Template;

abstract class Widget
{
    public $name;
    protected $template;


    /**
     * Widget constructor.
     *
     * @param TemplateService $templateService
     */
    public function __construct(TemplateService $templateService)
    {
        $this->template = $templateService->getTemplate();
    }

    /**
     * @param array $params
     * @return mixed
     */
    abstract function display($params = []);
}
